#use wml::debian::template title="Sites miroirs de Debian à travers le monde" BARETITLE=true
#use wml::debian::translation-check translation="54e7b1c853358a6c386f31e4ac3a5d90a6f3890f" maintainer="Jean-Paul Guillonneau "
# Translators :
# Nicolas Bertolissio
# Simon Paillard
# David Prévot
# Jean-Paul Guillonneau, 2016-2021
<p>
Debian est distribuée (<em>copiée</em>) sur des centaines de serveurs sur la
Toile. L'utilisation d'un serveur proche devrait augmenter la vitesse de
téléchargement et également réduire la charge de nos serveurs centraux et de la
Toile toute entière.
</p>

<p class="centerblock">
Des miroirs de Debian existent dans de nombreux pays et pour quelques-uns un
alias <code>ftp.&lt;pays&gt;.debian.org</code> a été ajouté. Cet alias pointe
généralement vers un miroir se synchronisant régulièrement et rapidement, et
proposant toutes les architectures de Debian. L’archive de Debian est toujours
disponible à l’aide d’<code>HTTP</code> à l’emplacement <code>/debian</code>
sur le serveur.
</p>

<p class="centerblock">
D’autres <strong>sites miroir</strong> peuvent contenir des restrictions sur ce
qu’ils proposent (à cause d’un manque d’espace). Qu’un site ne soit pas le
<code>ftp.&lt;pays&gt;.debian.org</code> du pays ne signifie pas
nécessairement qu’il soit plus lent ou moins à jour que le miroir
<code>ftp.&lt;pays&gt;.debian.org</code>. Dans les faits, un miroir qui
propose une architecture et qui est plus proche de vous, et donc plus rapide,
est toujours préférable aux autres miroirs plus éloignés.
</p>

<p>Le site le plus proche est à utiliser pour un téléchargement le plus rapide
possible que ce soit un alias de miroir relatif à un pays ou non. Le programme
<a href="https://packages.debian.org/stable/net/netselect">\
<em>netselect</em></a> peut être utilisé pour déterminer le site ayant la
plus faible latence. Un programme tel que
<a href="https://packages.debian.org/stable/web/wget"><em>wget</em></a> ou
<a href="https://packages.debian.org/stable/net/rsync"><em>rsync</em></a> peut
être utilisé pour déterminer le site ayant le plus fort débit. Remarquez que
la proximité géographique n’est pas souvent le facteur le plus important pour
connaître la machine qui vous servira le mieux.</p>

<p>
Si votre système se déplace beaucoup, vous pouvez être mieux servi par un
« miroir » qui est soutenu par un <abbr title="Content Delivery Network">CDN
</abbr> global (réseau de diffusion de contenu). Le projet Debian entretient
<code>deb.debian.org</code> dans ce but et vous pouvez l’utiliser dans votre
sources.list d’apt. Consulter <a href="http://deb.debian.org/"> le site web
du service pour les détails</a>.
</p>

<p>
Une copie officielle de la liste suivante est toujours disponible sur&nbsp;:
<url "https://www.debian.org/mirror/list" />.
Tout ce que vous souhaitez savoir d'autre sur les miroirs Debian se trouve
sur&nbsp;: <url "https://www.debian.org/mirror/" />.
</p>

<h2 class="center">Sites miroirs par pays de Debian</h2>

<table border="0" class="center">
<tr>
  <th>Pays</th>
  <th>Site</th>
  <th>Architectures</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-primary.inc"
</table>

<h2 class="center">Sites miroirs de l'archive Debian</h2>

<table border="0" class="center">
<tr>
  <th>Nom d'hôte</th>
  <th>HTTP</th>
  <th>Architectures</th>
</tr>
#include "$(ENGLISHDIR)/mirror/list-secondary.inc"
</table>

#include "$(ENGLISHDIR)/mirror/list-footer.inc"
