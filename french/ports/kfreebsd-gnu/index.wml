#use wml::debian::template title="Debian GNU/kFreeBSD" BARETITLE="true"

#use wml::debian::translation-check translation="6f3adf6374f35194686f89dec2ba66b1ecf3bb5f" maintainer="Baptiste Jammet"
#use wml::debian::toc

# Translators:
# Éric Madesclair, 2005.
# Frédéric Bothamy, 2006-2008.
# Simon Paillard, 2009.
# David Prévot, 2010, 2011.

<p>Debian GNU/kFreeBSD est un portage composé de
<a href="https://www.gnu.org/">l'espace utilisateur GNU</a> utilisant la
<a href="https://www.gnu.org/software/libc/">bibliothèque GNU C</a> au-dessus d'un 
noyau <a href="https://www.freebsd.org/">FreeBSD</a>, associé à
<a href="https://packages.debian.org/">l'ensemble des paquets Debian</a>.</p>

<div class="important">
<p>
Debian GNU/kFreeBSD n'est pas une architecture officiellement prise en charge.
Elle a été publiée avec Debian 6.0 (Squeeze) et 7.0 (Wheezy) sous forme
de démonstration technique, et premier portage non Linux.
Depuis Debian 8 (Jessie), elle n'est plus incluse dans les publications officielles.
</p>
</div>


<toc-add-entry name="resources">Resources</toc-add-entry>

<p>
Plus de renseignements au sujet du portage (y compris la FAQ) sont disponibles sur la page de wiki de
<a href="https://wiki.debian.org/fr/Debian_GNU/kFreeBSD">Debian GNU/kFreeBSD</a>.
</p>

<h3>Liste de diffusion</h3>

<p>
La <a href="https://lists.debian.org/debian-bsd">\
liste de diffusion Debian GNU/k*BSD</a>.
</p>

<h3>IRC</h3>

<p>
Le <a href="irc://irc.debian.org/#debian-kbsd">\
canal IRC #debian-kbsd</a> (sur irc.debian.org).
</p>


<toc-add-entry name="Development">Développement</toc-add-entry>

<p>
Avec l'utilisation de la Glibc les problèmes posés par
le portage sont simples. La plupart du temps, il suffit de copier pour 
"k*bsd*-gnu" un test conditionnel de type case depuis un autre système 
basé sur la Glibc (comme GNU ou GNU/Linux). Veuillez vous reporter au document 
sur le
<a href="https://salsa.debian.org/bsd-team/web/raw/master/porting/PORTING">portage</a>
pour plus d'informations.</p>

<p>Regardez aussi le fichier <a href="https://salsa.debian.org/bsd-team/web/raw/master/TODO">TODO</a>
pour plus de détails sur ce qui peut être fait.</p>


<toc-add-entry name="availablehw">Matériel à disposition des développeurs Debian</toc-add-entry>

<p>
lemon.debian.net (kfreebsd-amd64)
est à disposition des développeurs Debian pour leur travail de portage.

Veuillez vous référer à la <a href="https://db.debian.org/machines.cgi">base
de données des machines</a> pour plus de renseignements sur ces machines.

Normalement, vous devriez disposer de deux
environnements chroot : testing et unstable.

Remarquez que ces systèmes ne sont pas administrés par la DSA,
donc <b>n'envoyez pas les demandes relatives à debian-admin</b>.

À la place, utilisez <email "admin@lemon.debian.net">.
</p>
