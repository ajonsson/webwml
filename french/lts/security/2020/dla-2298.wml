#use wml::debian::translation-check translation="0c19b574725b51bf7147a0243c7ae57299d2a74b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs problèmes ont été découverts dans libapache2-mod-auth-openidc, le
module d’authentification de connexion d’OpenID pour le serveur HTTP Apache.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14857">CVE-2019-14857</a>

<p>Validation insuffisante d’URL conduisant à une vulnérabilité de redirection
ouverte. Un attaquant pouvait piéger une victime pour obtenir une
accréditation vers un fournisseur OpenID en redirigeant la requête vers un site
web non voulu.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-20479">CVE-2019-20479</a>

<p>Dû à une validation insuffisante d’URL, une vulnérabilité de redirection
ouverte pour les URL commençant avec une barre oblique ou une barre oblique
inverse pouvait être mal utilisée.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-1010247">CVE-2019-1010247</a>

<p>La page OIDCRedirectURI contenait du code JavaScript généré qui utilisait
un paramètre de scrutation comme variable de chaîne, pouvant donc contenir du
code JavaScript supplémentaire. Cela pouvait conduire à un script intersite (XSS).</p>


<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 2.1.6-1+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libapache2-mod-auth-openidc.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de libapache2-mod-auth-openidc, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc">https://security-tracker.debian.org/tracker/libapache2-mod-auth-openidc</a></p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p></li>

</ul>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2020/dla-2298.data"
# $Id: $
