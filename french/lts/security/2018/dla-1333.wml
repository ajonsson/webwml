#use wml::debian::translation-check translation="ec63203ae7c88b70ad002f6a6d89bd22297d4fb8" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Plusieurs vulnérabilités ont été découvertes dans le serveur de courrier
électronique Dovecot. Le projet « Common Vulnerabilities and Exposures »
(CVE) identifie les problèmes suivants.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-14461">CVE-2017-14461</a>

<p>Aleksandar Nikolic de Cisco Talos et <q>flxflndy</q> ont découvert que
Dovecot n'analysait pas correctement les adresses de courrier électronique
non valables, ce qui peut provoquer un plantage ou la divulgation de
contenus de mémoire à un attaquant.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15130">CVE-2017-15130</a>

<p>Les recherches de configuration TLS SNI pourraient conduire à une
utilisation de la mémoire excessive, faisant que la limite de la VSZ de
imap-login/pop3-login soit atteinte et que le processus redémarre, avec
pour conséquence un déni de service. Seules les configurations de Dovecot
contenant les blocs de configuration <code>local_name { }</code> ou
<code>local { }</code> sont affectées.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-15132">CVE-2017-15132</a>

<p>Dovecot renferme un défaut de fuite de mémoire dans le processus de
connexion lors d'une authentification SASL interrompue.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 1:2.1.7-7+deb7u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets dovecot.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1333.data"
# $Id: $
