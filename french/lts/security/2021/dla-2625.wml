#use wml::debian::translation-check translation="d376e818e49d363dfef6fd5c50f5cd01cc3d50ea" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Le paquet Debian courier-authlib avant la version 0.71.1-2 pour la
bibliothèque d’authentification de Courier crée un répertoire
/run/courier/authdaemon avec des permissions faibles, permettant à un attaquant
de lire des informations sur l’utilisateur. Cela peut comprendre le mot de
passe en clair dans certaines configurations. En général, cela inclut
l’existence de utilisateur, l’UID et le GID, les répertoires home ou Maildir,
les quota et certains types d’information sur le mot de passe (telle que
l’empreinte).</p>

<p>Pour Debian 9 <q>Stretch</q>, ce problème a été corrigé dans
la version 0.66.4-9+deb9u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets courier-authlib.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de courier-authlib, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/courier-authlib">\
https://security-tracker.debian.org/tracker/courier-authlib</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2625.data"
# $Id: $
