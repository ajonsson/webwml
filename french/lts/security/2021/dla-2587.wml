#use wml::debian::translation-check translation="dfd0e9eb094490e9b06281ec82715969a07d7c11" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans privoxy, un mandataire web
avec des capacités poussées de filtrage.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20272">CVE-2021-20272</a>

<p>Un échec d’assertion pourrait être provoqué par une requête CGI contrefaite
conduisant à un plantage du serveur.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20273">CVE-2021-20273</a>

<p>Un plantage peut se produire à l'aide d'une requête CGI contrefaite si
Privoxy est désactivé.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20275">CVE-2021-20275</a>

<p>Une lecture non valable de taille deux peut se produire dans
chunked_body_is_complete(), conduisant à un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-20276">CVE-2021-20276</a>

<p>Un accès en mémoire non valable avec un modèle non autorisé passé
à pcre_compile() pourrait conduire à déni de service.</p></li>

</ul>

<p>Pour Debian 9 <q>Stretch</q>, ces problèmes ont été corrigés dans
la version 3.0.26-3+deb9u2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets privoxy.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de privoxy, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/privoxy">\
https://security-tracker.debian.org/tracker/privoxy</a>.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment
posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2587.data"
# $Id: $
