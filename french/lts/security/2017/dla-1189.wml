#use wml::debian::translation-check translation="bc8feadbbc686ad5f9ceb695925a329dea1622c0" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Une vulnérabilité de sécurité mineure a été découverte dans Python 2.7, un
langage interactif de haut niveau orienté objet.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2017-1000158">CVE-2017-1000158</a>

<p>CPython (l’implémentation de référence de Python connue aussi communément
comme simplement Python) dans ses versions 2.6 et 2.7 est vulnérable à un
dépassement d'entier et une corruption de tas, conduisant à une possible exécution
arbitraire de code. La nature de l’erreur concerne le traitement impropre de
grandes chaînes avec des caractères d’échappement.</p></li>

</ul>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans
la version 2.7.3-6+deb7u4.</p>

<p>Nous vous recommandons de mettre à jour vos paquets python2.7.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify lea following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1189.data"
# $Id: $
