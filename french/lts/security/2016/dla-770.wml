#use wml::debian::translation-check translation="1d1c1ba842e225bf68a6fed5744786cc779234f7" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Dawid Golunski a découvert que PHPMailer, une bibliothèque populaire
pour envoyer des courriels à partir des applications PHP, permettait à un
attaquant distant d'exécuter du code s'il était capable de fournir une
adresse d'expéditeur contrefaite.</p>

<p>Notez que pour ce problème, le
<a href="https://security-tracker.debian.org/tracker/CVE-2016-10045">CVE-2016-10045</a>
a aussi été assigné, qui est une régression dans le correctif original
proposé pour le
<a href="https://security-tracker.debian.org/tracker/CVE-2016-10033">CVE-2016-10033</a>.
Comme le correctif original n'avait pas été appliqué dans Debian, Debian
n'était pas vulnérable au
<a href="https://security-tracker.debian.org/tracker/CVE-2016-10045">CVE-2016-10045</a>.</p>

<p>Pour Debian 7 <q>Wheezy</q>, ces problèmes ont été corrigés dans la
version 5.1-1.2.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libphp-phpmailer.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS,
comment appliquer ces mises à jour dans votre système et les questions
fréquemment posées peuvent être trouvées sur :
<a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2016/dla-770.data"
# $Id: $
