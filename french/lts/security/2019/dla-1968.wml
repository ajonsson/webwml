#use wml::debian::translation-check translation="409169ed41acc75d507a3f6c41c240c1fbb3dc04" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans imagemagick, une boîte
à outils de traitement d’image.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-11470">CVE-2019-11470</a>

<p>Consommation de ressources non contrôlée causée par une taille d’image
insuffisamment vérifiée dans ReadCINImage (coders/cin.c). Cette vulnérabilité
peut être exploitée par des attaquants distants pour provoquer un déni de
service à l'aide d'une image contrefaite Cineon.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-14981">CVE-2019-14981</a>

<p>Vulnérabilité de division par zéro dans MeanShiftImage (magick/feature.c).
Cette vulnérabilité peut être exploitée par des attaquants distants pour
provoquer un déni de service à l’aide de données d’image contrefaites.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15139">CVE-2019-15139</a>

<p>Lecture hors limites dans ReadXWDImage (coders/xwd.c). Cette vulnérabilité
peut être exploitée par des attaquants distants pour provoquer un déni de
service à l'aide d'un fichier d’image contrefait XWD (fichier de vidage d’écran
du système X Window).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-15140">CVE-2019-15140</a>

<p>Problème de vérification de limites dans ReadMATImage (coders/mat.c), conduisant
éventuellement à une utilisation de mémoire après libération. Cette
vulnérabilité peut être exploitée par des attaquants distants pour provoquer un
déni de service ou tout autre impact non précisé à l'aide d'un fichier d’image
contrefait MAT.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 8:6.8.9.9-5+deb8u18.</p>
<p>Nous vous recommandons de mettre à jour vos paquets imagemagick.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1968.data"
# $Id: $
