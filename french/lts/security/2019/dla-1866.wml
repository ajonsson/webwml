#use wml::debian::translation-check translation="0dd2b24ee1cc4df783cc5fc844d2ec6c86ccf93b" maintainer="Jean-Paul Guillonneau"
<define-tag description>Mise à jour de sécurité pour LTS</define-tag>
<define-tag moreinfo>

<p>Divers problèmes mineurs ont été corrigés dans la bibliothèque GLib. GLib est
une bibliothèque C d’usage général utilisée par des projets tels que GTK+, GIMP
et GNOME.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16428">CVE-2018-16428</a>

<p>Dans GNOME GLib, g_markup_parse_context_end_parse() dans gmarkup.c possède un
déréférencement de pointeur NULL.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-16429">CVE-2018-16429</a>

<p>GNOME GLib possède une vulnérabilité de lecture hors limites dans
g_markup_parse_context_parse() dans gmarkup.c, concernant utf8_str().</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-13012">CVE-2019-13012</a>

<p>Le dorsal de réglages de fichier de clefs dans GNOME GLib (c'est-à-dire glib2.0)
avant 2.60.0 créait des répertoires en utilisant
g_file_make_directory_with_parents (kfsb->dir, NULL, NULL) et des fichiers en
utilisant g_file_replace_contents (kfsb->file, contents, length, NULL, FALSE,
G_FILE_CREATE_REPLACE_DESTINATION, NULL, NULL, NULL). Par conséquent, il ne
restreignait pas correctement les permissions de répertoire (et de fichier). Pour
les répertoires, la permission 0777 était utilisée, pour les fichiers, les
permissions par défaut étaient utilisées. Ce problème est similaire
à <a href="https://security-tracker.debian.org/tracker/CVE-2019-12450">CVE-2019-12450</a>.</p></li>

</ul>

<p>Pour Debian 8 <q>Jessie</q>, ces problèmes ont été corrigés dans
la version 2.42.1-1+deb8u2.</p>
<p>Nous vous recommandons de mettre à jour vos paquets glib2.0.</p>

<p>Plus d’informations à propos des annonces de sécurité de Debian LTS, comment
appliquer ces mises à jour dans votre système et les questions fréquemment posées
peuvent être trouvées sur : <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1866.data"
# $Id: $
