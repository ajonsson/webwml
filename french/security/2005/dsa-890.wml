#use wml::debian::translation-check translation="793ef2aaf7ac1e7953d45aed262afca75ae7986e" maintainer="Simon Paillard"
<define-tag description>Plusieurs vulnérabilités</define-tag>
<define-tag moreinfo>
<p>Chris Evans a découvert plusieurs vulnérabilités dans libungif4, une
bibliothèque dédiée aux images GIF. Le projet «&nbsp;Common Vulnerabilities and
Exposures&nbsp;» a identifié les problèmes suivants&nbsp;:</p>

<ul>

<li><a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2005-2974">CVE-2005-2974</a>

    <p>Un déréférencement de pointeur nul pouvait provoquer un déni de
    service.</p></li>

<li><a href="https://cve.mitre.org/cgi-bin/cvename.cgi?name=CVE-2005-3350">CVE-2005-3350</a>

    <p>Des accès à la mémoire au-delà des limites pouvaient provoquer un déni
    de service ou permettre l'exécution de code arbitraire.</p></li>

</ul>

<p>Pour l'ancienne distribution stable (<em>Woody</em>), ces problèmes ont été
corrigés dans la version&nbsp;4.1.0b1-2woody1.</p>

<p>Pour l'actuelle distribution stable (<em>Sarge</em>), ces problèmes ont été
corrigés dans la version&nbsp;4.1.3-2sarge1.</p>

<p>Pour la distribution instable (<em>Sid</em>), ces problèmes seront bientôt
corrigés.</p>

<p>Nous vous recommandons de mettre à jour vos paquets libungif4.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2005/dsa-890.data"
