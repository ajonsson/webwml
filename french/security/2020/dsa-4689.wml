#use wml::debian::translation-check translation="139ced0522f792565594fd4bc65bf27ae29bd20d" maintainer="Jean-Pierre Giraud"
<define-tag description>Mise à jour de sécurité</define-tag>
<define-tag moreinfo>
<p>Plusieurs vulnérabilités ont été découvertes dans BIND, une
implémentation de serveur DNS.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2019-6477">CVE-2019-6477</a>

<p>Les requêtes acheminées par TCP peuvent contourner les limites du client
TCP avec pour conséquence un déni de service.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8616">CVE-2020-8616</a>

<p>BIND ne limite pas suffisamment le nombre de récupérations réalisées
lors du traitement de références. Un attaquant peut tirer avantage de ce
défaut pour provoquer un déni de service (dégradation de performance) ou
pour utiliser le serveur de récursion dans une attaque par réflexion avec
un haut facteur d'amplification.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2020-8617">CVE-2020-8617</a>

<p>Une erreur logique dans le code qui vérifie la validité TSIG peut être
utilisée pour déclencher un échec d'assertion, avec pour conséquence un
déni de service.</p></li>

</ul>

<p>Pour la distribution oldstable (Stretch), ces problèmes ont été corrigés
dans la version 1:9.10.3.dfsg.P4-12.3+deb9u6.</p>

<p>Pour la distribution stable (Buster), ces problèmes ont été corrigés
dans la version 1:9.11.5.P4+dfsg-5.1+deb10u1.</p>

<p>Nous vous recommandons de mettre à jour vos paquets bind9.</p>

<p>Pour disposer d'un état détaillé sur la sécurité de bind9, veuillez
consulter sa page de suivi de sécurité à l'adresse :
<a href="https://security-tracker.debian.org/tracker/bind9">\
https://security-tracker.debian.org/tracker/bind9</a>.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2020/dsa-4689.data"
# $Id: $
