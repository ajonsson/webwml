<define-tag pagetitle>De officiële communicatiekanalen van Debian</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news
#use wml::debian::translation-check translation="c1e997409ebbfee3f612efc60c0f1e3fe81c7e9c" maintainer="Frans Spiesschaer"

<p>
Af en toe krijgen we in Debian vragen over onze officiële
communicatiekanalen, alsook vragen over de Debian-status van wie eigenaar
is van websites met een gelijkaardige naam.
</p>

<p>
De hoofdwebsite van Debian,
<a href="https://www.debian.org">www.debian.org</a>,
is ons belangrijkste communicatiemedium. Wie informatie zoekt over actuele
gebeurtenissen en de ontwikkelingsvoortgang in de gemeenschap is mogelijk
geïnteresseerd in de
<a href="https://www.debian.org/News/">Debian Nieuws</a>-sectie van de website van Debian.

Voor minder formele aankondigingen gebruiken we de officiële Debian blog
<a href="https://bits.debian.org">Bits from Debian</a>,
en voor kortere nieuwsitems de dienst <a href="https://micronews.debian.org">Debian micronews</a>.
</p>

<p>
Onze officiële nieuwsbrief
<a href="https://www.debian.org/News/weekly/">Debian Projectnieuws</a>
en alle officiële aankondigingen van nieuwsfeiten of veranderingen in het
project, worden zowel gepost op onze website als in onze officiële
mailinglijsten,
<a href="https://lists.debian.org/debian-announce/">debian-announce</a> of
<a href="https://lists.debian.org/debian-news/">debian-news</a>.
Posten op die mailinglijsten is niet zomaar toegelaten.
</p>

<p>
We willen ook van de gelegenheid gebruik maken om toe te lichten hoe het
Debian Project, of in het kort, Debian gestructureerd is.
</p>

<p>
De structuur van Debian wordt gereguleerd in onze
<a href="https://www.debian.org/devel/constitution">Statuten</a>.
Verantwoordelijken en gedelegeerde leden worden vermeld op de pagina over
onze <a href="https://www.debian.org/intro/organization">Organisatiestructuur</a>.
Andere teams worden vermeld op onze <a href="https://wiki.debian.org/Teams">Teams</a>-pagina van de wiki.
</p>

<p>
De volledige lijst van officiële leden van Debian is te vinden op onze
pagina over <a href="https://nm.debian.org/members">Nieuwe Leden</a>,
waar het lidmaatschapsbeheer plaats vindt. Een ruimere lijst van mensen die
bijdragen aan Debian is te vinden op onze
<a href="https://contributors.debian.org">Medewerkers</a>-pagina.
</p>

<p>
Indien u vragen heeft, nodigen we u uit contact op te nemen met het
persteam op
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

<h2>Over Debian</h2>
<p>Het Debian Project is een vereniging van ontwikkelaars van Vrije
Software die op vrijwillige basis tijd en energie investeren in het
ontwikkelen van het volledig vrije Debian besturingssysteem.</p>

<h2>Contactinformatie</h2>
<p>Raadpleeg voor bijkomende informatie de webpagina's van Debian op
<a href="$(HOME)/">https://www.debian.org/</a>, of stuur een e-mail naar
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>

