#use wml::debian::template title="Debian 9 -- Errata" BARETITLE=true
#use wml::debian::toc

#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="ba4da87abca9b144e1cc1ff151bf45311e5da2b1"
# Translator: Holger Wansing <hwansing@mailbox.org>, 2016, 2017, 2018, 2019, 2020.

<toc-display/>


# <toc-add-entry name="known_probs">Bekannte Probleme</toc-add-entry>

<toc-add-entry name="security">Sicherheitsprobleme</toc-add-entry>

<p>Das Debian-Sicherheitsteam stellt Aktualisierungen von Paketen in der
stabilen Veröffentlichung bereit, in denen Sicherheitsprobleme erkannt wurden.
Bitte lesen Sie die <a href="$(HOME)/security/">Sicherheitsseiten</a>
bezüglich weiterer Informationen über alle Sicherheitsprobleme, die in
<q>Stretch</q> erkannt wurden.</p>

<p>Wenn Sie APT verwenden, fügen Sie die folgende Zeile zu
<tt>/etc/apt/sources.list</tt> hinzu, um auf die neuesten
Sicherheitsaktualisierungen zugreifen zu können:

<pre>
  deb http://security.debian.org/ stretch/updates main contrib non-free
</pre>

<p>Führen Sie danach <kbd>apt-get update</kbd> gefolgt von
<kbd>apt-get upgrade</kbd> aus.</p>


<toc-add-entry name="pointrelease">Zwischenveröffentlichungen</toc-add-entry>

<p>Manchmal wird die veröffentlichte Distribution aktualisiert, wenn mehrere
   kritische Probleme aufgetreten oder Sicherheitsaktualisierungen
   herausgebracht worden sind. Im
   Allgemeinen wird dies als Zwischenveröffentlichung bezeichnet.</p>

<ul>
  <li>Die erste Zwischenveröffentlichung (9.1) wurde am
      <a href="$(HOME)/News/2017/20170722">22. Juli 2017</a> veröffentlicht.</li>
  <li>Die zweite Zwischenveröffentlichung (9.2) wurde am
      <a href="$(HOME)/News/2017/20171007">07. Oktober 2017</a> veröffentlicht.</li>
  <li>Die dritte Zwischenveröffentlichung (9.3) wurde am
      <a href="$(HOME)/News/2017/2017120902">09. Dezember 2017</a> veröffentlicht.</li>
  <li>Die vierte Zwischenveröffentlichung (9.4) wurde am
      <a href="$(HOME)/News/2018/20180310">10. März 2018</a> veröffentlicht.</li>
  <li>Die fünfte Zwischenveröffentlichung (9.5) wurde am
      <a href="$(HOME)/News/2018/20180714">14. Juli 2018</a> veröffentlicht.</li>
  <li>Die sechste Zwischenveröffentlichung (9.6) wurde am
      <a href="$(HOME)/News/2018/20181110">10. November 2018</a> veröffentlicht.</li>
  <li>Die siebte Zwischenveröffentlichung (9.7) wurde am
      <a href="$(HOME)/News/2019/20190123">23. Januar 2019</a> veröffentlicht.</li>
  <li>Die achte Zwischenveröffentlichung (9.8) wurde am
      <a href="$(HOME)/News/2019/20190216">16. Februar 2019</a> veröffentlicht.</li>
  <li>Die neunte Zwischenveröffentlichung (9.9) wurde am
      <a href="$(HOME)/News/2019/20190427">27. April 2019</a> veröffentlicht.</li>
  <li>Die zehnte Zwischenveröffentlichung (9.10) wurde am
      <a href="$(HOME)/News/2019/2019090702">07. September 2019</a> veröffentlicht.</li>
  <li>Die elfte Zwischenveröffentlichung (9.11) wurde am
      <a href="$(HOME)/News/2019/20190908">08. September 2019</a> veröffentlicht.</li>
 <li>Die zwölfte Zwischenveröffentlichung (9.12) wurde am
      <a href="$(HOME)/News/2020/2020020802">08. Februar 2020</a> veröffentlicht.</li>
 <li>Die dreizehnte Zwischenveröffentlichung (9.13) wurde am
      <a href="$(HOME)/News/2020/20200718">18. Juli 2020</a> veröffentlicht.</li>

</ul>

<ifeq <current_release_stretch> 9.0 "

<p>Es gibt derzeit noch keine Zwischenveröffentlichungen für Debian 9.</p>" "

<p>Details über die Änderungen zwischen 9.0 und <current_release_stretch/>
finden Sie im <a
href=http://http.us.debian.org/debian/dists/stretch/ChangeLog>\
Änderungsprotokoll (Changelog)</a>.</p>"/>

<p>Korrekturen für die veröffentlichte stabile Distribution durchlaufen oft
eine ausgedehnte Testperiode, bevor sie im Archiv akzeptiert werden. Diese
Korrekturen sind allerdings im Verzeichnis
<a href="http://ftp.debian.org/debian/dists/stretch-proposed-updates/">\
dists/stretch-proposed-updates</a> jedes Debian-Archiv-Spiegels verfügbar.</p>

<p>Falls Sie APT zur Aktualisierung Ihrer Pakete verwenden, können Sie diese
vorgeschlagenen Änderungen (proposed-updates) installieren, indem Sie folgende
Zeilen zu <tt>/etc/apt/sources.list</tt> hinzufügen:</p>

<pre>
  \# vorgeschlagene Änderungen für eine Debian 9 Zwischenveröffentlichung
  deb http://ftp.us.debian.org/debian stretch-proposed-updates main contrib non-free
</pre>

<p>Führen Sie danach <kbd>apt-get update</kbd> gefolgt von
<kbd>apt-get upgrade</kbd> aus.</p>


<toc-add-entry name="installer">Installationssystem</toc-add-entry>

<p>
Informationen über Errata und Aktualisierungen für das Installationssystem
finden Sie auf der <a href="debian-installer/">Webseite zum Debian-Installer</a>.
</p>
