#use wml::debian::translation-check translation="846effa858f46cf6429f61e241ec96292032972f" maintainer="Erik Pfannenstein"
<define-tag pagetitle>Debian 10 aktualisiert: 10.5 veröffentlicht</define-tag>
<define-tag release_date>2020-08-01</define-tag>
#use wml::debian::news

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.5</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>


<p>
Das Debian-Projekt freut sich, die fünfte Aktualisierung seiner 
Stable-Veröffentlichung Debian <release> (Codename <q><codename></q>) 
ankündigen zu dürfen. Diese Aktualisierung behebt hauptsächlich 
Sicherheitslücken der Stable-Veröffentlichung sowie einige ernste Probleme. 
Für sie sind bereits separate Sicherheitsankündigungen veröffentlicht worden, auf 
die, wenn möglich, verwiesen wird.
</p>

<p>
Diese Zwischenveröffentlichung behandelt auch die folgende Sicherheitsankündigung: 
<a href="https://www.debian.org/security/2020/dsa-4735">DSA-4735-1 grub2 -- security update</a> 
In ihr geht es um mehrere Sicherheitslücken, die unter dem Namen 
<a href="https://www.debian.org/security/2020-GRUB-UEFI-SecureBoot/">GRUB2 UEFI SecureBoot ›BootHole‹</a> zusammengefasst wurden.
</p>

<p>
Bitte beachten Sie, dass diese Aktualisierung keine neue Version von 
Debian <release> darstellt, sondern nur einige der enthaltenen Pakete 
auffrischt. Es gibt keinen Grund, <codename>-Medien zu entsorgen, da deren 
Pakete nach der Installation mit Hilfe eines aktuellen Debian-Spiegelservers 
auf den neuesten Stand gebracht werden können. 
</p>

<p>
Wer häufig Aktualisierungen von security.debian.org herunterlädt, wird 
nicht viele Pakete auf den neuesten Stand bringen müssen. Die meisten dieser 
Aktualisierungen sind in dieser Revision enthalten.
</p>

<p>
Neue Installationsabbilder können bald von den gewohnten Orten bezogen werden. 
</p>

<p>
Vorhandene Installationen können auf diese Revision angehoben werden, indem 
das Paketverwaltungssystem auf einen der vielen HTTP-Spiegel von Debian 
verwiesen wird. Eine vollständige Liste der Spiegelserver ist verfügbar unter: 
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Verschiedene Fehlerkorrekturen</h2>

<p>Diese Stable-Veröffentlichung nimmt an den folgenden Paketen einige wichtige 
Korrekturen vor:</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction appstream-glib "Kompilierungsfehlschlag im Jahr 2020 und später behoben">
<correction asunder "Standardmäßig gnudb statt freedb verwenden">
<correction b43-fwcutter "Sicherstellen, dass die Entfernung unter nicht-englischen Locales funktioniert; Entfernung nicht fehlschlagen lassen, wenn einige Dateien nicht mehr existieren; fehlende Abhängigkeiten von pciutils und ca-certificates ergänzt">
<correction balsa "Beim Validieren von Zertifikaten die Server-Identität mitteilen, sodass die Validierung klappt, wenn die glib-networking-Korrektur für CVE-2020-13645 verwendet wird">
<correction base-files "Aktualisierung auf die Zwischenveröffentlichung">
<correction batik "Serverseitige Abfragefälschung via xlink:href-Attribut behoben [CVE-2019-17566]">
<correction borgbackup "Fehler beseitigt, der den Index ruiniert und so zu Datenverlust führt">
<correction bundler "Erforderliche Version der ruby-molinillo geändert">
<correction c-icap-modules "Unterstützung für ClamAV 0.102 hinzugefügt">
<correction cacti "Problem behoben, dass UNIX-Zeitstempel nach dem 13. September 2020 als Graph-Start oder -Ende abgelehnt wurden; Code-Fernausführung behoben [CVE-2020-7237], Seitenübergreifendes Scripting behoben [CVE-2020-7106], genauso ein CSRF-Problem [CVE-2020-13231]; Deaktivierung eines Benutzerkontos sperrt nicht automatisch seine Berechtigungen [CVE-2020-13230]">
<correction calamares-settings-debian "Displaymanager-Modul freigeschaltet, um Autologin-Optionen zu berichtigen; xdg-user-dir zum Angeben des Desktop-Verzeichnisses verwenden">
<correction clamav "Neue Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2020-3327 CVE-2020-3341 CVE-2020-3350 CVE-2020-3327 CVE-2020-3481]">
<correction cloud-init "Neue Veröffentlichung der Originalautoren">
<correction commons-configuration2 "Erzeugung von Objekten beim Laden von YAML-Dateien verhindern [CVE-2020-1953]">
<correction confget "Umgang des Python-Modules mit Werten mit <q>=</q> überarbeitet">
<correction dbus "Neue stabile Veröffentlichung der Originalautoren; Anfälligkeit für Dienstblockade abgeschafft [CVE-2020-12049]; Use-after-free, wenn zwei Benutzernamen sich eine UID teilen, verhindert">
<correction debian-edu-config "Verlust der dynamisch allozierten IPv4-Adresse behoben">
<correction debian-installer "Linux-ABI auf 4.19.0-10 aktualisiert">
<correction debian-installer-netboot-images "Neukompilierung gegen proposed-updates">
<correction debian-ports-archive-keyring "Ablaufdatum des 2020-Schlüssels (84C573CD4E1AFD6C) um ein Jahr nach hinten verschoben; Debian Ports Archive Automatic Signing Key hinzugefügt (2021); 2018er Schlüssel (ID: 06AED62430CB581C) auf den Entfernt-Schlüsselbund verschoben">
<correction debian-security-support "Unterstützungs-Status für diverse Pakete aktualisiert">
<correction dpdk "Neue Veröffentlichung der Originalautoren">
<correction exiv2 "Über-restriktive Sicherheitskorrektur nachjustiert [CVE-2018-10958 und CVE-2018-10999]; Dienstblockade-Problem beseitigt [CVE-2018-16336]">
<correction fdroidserver "Litecoin-Adressüberprüfung überarbeitet">
<correction file-roller "Sicherheitskorrektur [CVE-2020-11736]">
<correction freerdp2 "Smartcard-Anmeldungen überarbeitet; Sicherheitskorrekturen [CVE-2020-11521 CVE-2020-11522 CVE-2020-11523 CVE-2020-11524 CVE-2020-11525 CVE-2020-11526]">
<correction fwupd "Neue Veröffentlichung der Originalautoren; mögliches Problem bei der Signaturverifizierung behoben [CVE-2020-10759]; rotierte Debian-Signierschlüssel verwenden">
<correction fwupd-amd64-signed "Neue Veröffentlichung der Originalautoren; mögliches Problem bei der Signaturverifizierung behoben [CVE-2020-10759]; rotierte Debian-Signierschlüssel verwenden">
<correction fwupd-arm64-signed "Neue Veröffentlichung der Originalautoren; mögliches Problem bei der Signaturverifizierung behoben [CVE-2020-10759]; rotierte Debian-Signierschlüssel verwenden">
<correction fwupd-armhf-signed "Neue Veröffentlichung der Originalautoren; mögliches Problem bei der Signaturverifizierung behoben [CVE-2020-10759]; rotierte Debian-Signierschlüssel verwenden">
<correction fwupd-i386-signed "Neue Veröffentlichung der Originalautoren; mögliches Problem bei der Signaturverifizierung behoben [CVE-2020-10759]; rotierte Debian-Signierschlüssel verwenden">
<correction fwupdate "Rotierte Debian-Signierschlüssel verwenden">
<correction fwupdate-amd64-signed "Rotierte Debian-Signierschlüssel verwenden">
<correction fwupdate-arm64-signed "Rotierte Debian-Signierschlüssel verwenden">
<correction fwupdate-armhf-signed "Rotierte Debian-Signierschlüssel verwenden">
<correction fwupdate-i386-signed "Rotierte Debian-Signierschlüssel verwenden">
<correction gist "Veraltete Autorisierungs-API vermeiden">
<correction glib-networking "Nicht gesetzte Identität als Identitätsfehler melden [CVE-2020-13645]; balsa-Versionen vor 2.5.6-2+deb10u1 als defekt betrachten, weil die Korrektur für CVE-2020-13645 balsas Zertifikatsverifizierung beschädigt">
<correction gnutls28 "TLS1.2-Wiederaufnahmefehler behoben; Speicherleck behoben; Sitzungstickets mit Länge null akzeptieren, um Verbindungsprobleme bei TLS1.2-Sitzungen mit einigen großen Hosting-Anbietern zu beheben; Überprüfungsfehler mit alternativer Zertifikatskette behoben">
<correction intel-microcode "Einige Microcodes durch frühere Versionen ersetzt, um Hänger beim Hochfahren von Skylake-U/Y und Skylake Xeon E3 zu vermeiden">
<correction jackson-databind "Mehrere Sicherheitsprobleme betreffend BeanDeserializerFactory behoben [CVE-2020-9548 CVE-2020-9547 CVE-2020-9546 CVE-2020-8840 CVE-2020-14195 CVE-2020-14062 CVE-2020-14061 CVE-2020-14060 CVE-2020-11620 CVE-2020-11619 CVE-2020-11113 CVE-2020-11112 CVE-2020-11111 CVE-2020-10969 CVE-2020-10968 CVE-2020-10673 CVE-2020-10672 CVE-2019-20330 CVE-2019-17531 and CVE-2019-17267]">
<correction jameica "mckoisqldb zum classpath hinzufügen, sodass das SynTAX-Plugin verwendet werden kann">
<correction jigdo "HTTPS-Unterstützung in jigdo-lite und jigdo-mirror überarbeitet">
<correction ksh "Problem mit Begrenzungen der Umgebungsvariablen behoben [CVE-2019-14868]">
<correction lemonldap-ng "Regression in der nginx-Konfiguration behoben, welche durch die Korrektur für CVE-2019-19791 verursacht wurde">
<correction libapache-mod-jk "Apache-Konfigurationsdatei umbenennen, sodass sie automatisch aktiviert und deaktiviert werden kann">
<correction libclamunrar "Neue stabile Veröffentlichung der Originalautoren; unversioniertes Metapaket hinzugefügt">
<correction libembperl-perl "Umgang mit Fehlerseiten von Apache &gt;= 2.4.40 verbessert">
<correction libexif "Sicherheitskorrekturen [CVE-2020-12767 CVE-2020-0093 CVE-2020-13112 CVE-2020-13113 CVE-2020-13114]; Pufferüberlauf [CVE-2020-0182] und Ganzzahlüberlauf behoben [CVE-2020-0198]">
<correction libinput "Quirks: Trackpoint-Integrationsattribut hinzugefügt">
<correction libntlm "Pufferüberlauf behoben [CVE-2019-17455]">
<correction libpam-radius-auth "Pufferüberlauf im Passwortfeld behoben [CVE-2015-9542]">
<correction libunwind "Speicherzugriffsfehler auf mips behoben; Unterstützung für C++-Ausnahmen nur auf i386 und amd64 per Hand aktiviert">
<correction libyang "Absturz wegen Cache-Korrumpierung behoben, CVE-2019-19333, CVE-2019-19334">
<correction linux "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-latest "Aktualisierung für Kernel-ABI 4.19.0-10">
<correction linux-signed-amd64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-arm64 "Neue stabile Veröffentlichung der Originalautoren">
<correction linux-signed-i386 "Neue stabile Veröffentlichung der Originalautoren">
<correction lirc "Verwaltung der Konfigurationsdateien überarbeitet">
<correction mailutils "maidag: Setuid-Privilegien für alle Lieferoperationen außer mda abgeben [CVE-2019-18862]">
<correction mariadb-10.3 "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2020-2752 CVE-2020-2760 CVE-2020-2812 CVE-2020-2814 CVE-2020-13249]; Regression in RocksDB-ZSTD-Suche behoben">
<correction mod-gnutls "Möglichen Speicherzugriffsfehler bei fehlgeschlagenem TLS-Handshake behoben; Testfehlschläge behoben">
<correction multipath-tools "kpartx: richtigen Pfad zu partx in der udev-Regel verwenden">
<correction mutt "IMAP-PREAUTH-Verschlüsselung nicht überprüfen, wenn $tunnel verwendet wird">
<correction mydumper "Verknüpfung mit libm">
<correction nfs-utils "statd: user-id aus /var/lib/nfs/sm entnehmen [CVE-2019-3689]; statd nicht zum Besitzer von /var/lib/nfs machen">
<correction nginx "Anfälligkeit für Anfrageschmuggel auf Fehlerseiten behoben [CVE-2019-20372]">
<correction nmap "Standard-Schlüssellänge auf 2048 Bit geändert">
<correction node-dot-prop "Regression behoben, die durch die Korrektur von CVE-2020-8116 verursacht wurde">
<correction node-handlebars "Direkten Aufruf von <q>helperMissing</q> und <q>blockHelperMissing</q> unterbunden [CVE-2019-19919]">
<correction node-minimist "Prototype Pollution behoben [CVE-2020-7598]">
<correction nvidia-graphics-drivers "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2020-5963 CVE-2020-5967]">
<correction nvidia-graphics-drivers-legacy-390xx "Neue stabile Veröffentlichung der Originalautoren; Sicherheitskorrekturen [CVE-2020-5963 CVE-2020-5967]">
<correction openstack-debian-images "resolvconf bei der Installation von cloud-init mitinstallieren">
<correction pagekite "Probleme, die mit dem Auslaufen der mitgelieferten SSL-Zertifikate zusammenhängen, verhindet, indem die aus dem ca-certifcates-Paket benutzt werden">
<correction pdfchain "Absturz beim Programmstart behoben">
<correction perl "Mehrere Sicherheitskorrekturen im Zusammenhang mit regulären Ausdrücken [CVE-2020-10543 CVE-2020-10878 CVE-2020-12723]">
<correction php-horde "Anfälligkeit für seitenübergreifendes Skripting beseitigt [CVE-2020-8035]">
<correction php-horde-gollem "Anfälligkeit für seitenübergreifendes Skripting in Brotkrumen-Ausgabe beseitigt [CVE-2020-8034]">
<correction pillow "Mehrere Probleme mit Lesezugriff außerhalb der Grenzen behoben [CVE-2020-11538 CVE-2020-10378 CVE-2020-10177]">
<correction policyd-rate-limit "Accounting-Probleme durch Socket-Wiederverwendung behoben">
<correction postfix "Neue stabile Veröffentlichung der Originalautoren; Speicherzugriffsfehler in der tlsproxy-Client-Rolle behoben, wo die Serverrolle abgeschaltet war;  <q>maillog_file_rotate_suffix default value used the minute instead of the month</q> (maillog_file_rotate_suffix-Standardwert hatte die Minute statt des Monats) behoben; mehrere Probleme mit TLS behoben; README.Debian überarbeitet">
<correction python-markdown2 "Problem mit seitenübergreifendem Skripting behoben [CVE-2020-11888]">
<correction python3.7 "Endlosschleife bei Verwendung des tarfile-Moduls und gezielt angefertigten TAR-Dateien behoben [CVE-2019-20907]; Hash-Kollisionen für IPv4Interface und IPv6Interface behoben [CVE-2020-14422]; Dienstblockade in urllib.request.AbstractBasicAuthHandler behoben [CVE-2020-8492]">
<correction qdirstat "Speicherung der benutzerkonfigurierten MIME-Kategorien überarbeitet">
<correction raspi3-firmware "Tippfehler, der das System am Booten hindern kann, korrigiert">
<correction resource-agents "IPsrcaddr: <q>proto</q> optional machen, um Regression zu beheben, wenn das Programm ohne NetworkManager verwendet wird">
<correction ruby-json "Anfälligkeit für unsichere Objekterzeugung behoben [CVE-2020-10663]">
<correction shim "Rotierte Debian-Signierschlüssel verwenden">
<correction shim-helpers-amd64-signed "Rotierte Debian-Signierschlüssel verwenden">
<correction shim-helpers-arm64-signed "Rotierte Debian-Signierschlüssel verwenden">
<correction shim-helpers-i386-signed "Rotierte Debian-Signierschlüssel verwenden">
<correction speedtest-cli "Die richtigen Kopfzeilen verwenden, sodass der Upload-Geschwindigkeitstest richtig funktioniert">
<correction ssvnc "Schreibzugriff außerhalb der Grenzen behoben [CVE-2018-20020], außerdem eine Endlosschleife [CVE-2018-20021], unordentliche Initialisierung [CVE-2018-20022] und eine potenzielle Dienstblockade [CVE-2018-20024]">
<correction storebackup "Mögliche Anfälligkeit für Privilegieneskalation behoben [CVE-2020-7040]">
<correction suricata "Fallenlassen der Privilegien im nflog-Ausführungsmodus überarbeitet">
<correction tigervnc "libunwind nicht auf armel, armhf oder arm64 verwenden">
<correction transmission "Potenzielle Dienstblockade behoben [CVE-2018-10756]">
<correction wav2cdr "C99-Ganzzahltypen fester Länge verwenden, um Laufzeit-Assertion auf Nicht-amd64 und -alpha zu korrigieren">
<correction zipios++ "Sicherheitskorrektur [CVE-2019-13453]">
</table>


<h2>Sicherheitsaktualisierungen</h2>

<p>Diese Revision fügt der Stable-Veröffentlichung die folgenden 
Sicherheitsaktualisierungen hinzu. Das Sicherheitsteam hat bereits für jede 
davon eine Ankündigung veröffentlicht:</p>

<table border=0>
<tr><th>Ankündigungs-ID</th>  <th>Paket</th></tr>
<dsa 2020 4626 php7.3>
<dsa 2020 4674 roundcube>
<dsa 2020 4675 graphicsmagick>
<dsa 2020 4676 salt>
<dsa 2020 4677 wordpress>
<dsa 2020 4678 firefox-esr>
<dsa 2020 4679 keystone>
<dsa 2020 4680 tomcat9>
<dsa 2020 4681 webkit2gtk>
<dsa 2020 4682 squid>
<dsa 2020 4683 thunderbird>
<dsa 2020 4684 libreswan>
<dsa 2020 4685 apt>
<dsa 2020 4686 apache-log4j1.2>
<dsa 2020 4687 exim4>
<dsa 2020 4688 dpdk>
<dsa 2020 4689 bind9>
<dsa 2020 4690 dovecot>
<dsa 2020 4691 pdns-recursor>
<dsa 2020 4692 netqmail>
<dsa 2020 4694 unbound>
<dsa 2020 4695 firefox-esr>
<dsa 2020 4696 nodejs>
<dsa 2020 4697 gnutls28>
<dsa 2020 4699 linux-signed-amd64>
<dsa 2020 4699 linux-signed-arm64>
<dsa 2020 4699 linux-signed-i386>
<dsa 2020 4699 linux>
<dsa 2020 4700 roundcube>
<dsa 2020 4701 intel-microcode>
<dsa 2020 4702 thunderbird>
<dsa 2020 4704 vlc>
<dsa 2020 4705 python-django>
<dsa 2020 4707 mutt>
<dsa 2020 4708 neomutt>
<dsa 2020 4709 wordpress>
<dsa 2020 4710 trafficserver>
<dsa 2020 4711 coturn>
<dsa 2020 4712 imagemagick>
<dsa 2020 4713 firefox-esr>
<dsa 2020 4714 chromium>
<dsa 2020 4716 docker.io>
<dsa 2020 4718 thunderbird>
<dsa 2020 4719 php7.3>
<dsa 2020 4720 roundcube>
<dsa 2020 4721 ruby2.5>
<dsa 2020 4722 ffmpeg>
<dsa 2020 4723 xen>
<dsa 2020 4724 webkit2gtk>
<dsa 2020 4725 evolution-data-server>
<dsa 2020 4726 nss>
<dsa 2020 4727 tomcat9>
<dsa 2020 4728 qemu>
<dsa 2020 4729 libopenmpt>
<dsa 2020 4730 ruby-sanitize>
<dsa 2020 4731 redis>
<dsa 2020 4732 squid>
<dsa 2020 4733 qemu>
<dsa 2020 4735 grub-efi-amd64-signed>
<dsa 2020 4735 grub-efi-arm64-signed>
<dsa 2020 4735 grub-efi-ia32-signed>
<dsa 2020 4735 grub2>
</table>


<h2>Entfernte Pakete</h2>
<p>
Die folgenden Pakete wurden wegen Umständen entfernt, die außerhalb unserer 
Kontrolle liegen:
</p>

<table border=0>
<tr><th>Paket</th>               <th>Grund</th></tr>
<correction golang-github-unknwon-cae "Sicherheitsprobleme; unbetreut">
<correction janus "Keine Unterstützung in Stable möglich">
<correction mathematica-fonts "Verlässt sich auf nicht verfügbare Download-Quelle">
<correction matrix-synapse "Sicherheitsproblme; nicht unterstützungsfähig">
<correction selenium-firefoxdriver "Inkompatibel mit neueren Firefox-ESR-Versionen">
</table>

<h2>Debian-Installer</h2>
<p>
Der Installer wurde neu gebaut, damit er die Sicherheitskorrekturen enthält, 
die durch diese Zwischenveröffentlichung in Stable eingeflossen sind.
</p>


<h2>URLs</h2>

<p>Die vollständige Liste von Paketen, die sich mit dieser Revision geändert 
haben:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>Die derzeitige Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Vorgeschlagene Aktualisierungen für die Stable-Distribution:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Informationen zur Stable-Distribution (Veröffentlichungshinweise, Errata 
usw.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Sicherheitsankündigungen und -informationen:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>Über Debian</h2>

<p>Das Debian-Projekt ist ein Zusammenschluss von Entwicklern Freier Software, 
die ihre Zeit und Bemühungen einbringen, um das vollständig freie 
Betriebssystem Debian zu erschaffen.</p>

<h2>Kontaktinformationen</h2>

<p>Für weitere Informationen besuchen Sie bitte die Debian-Webseiten unter
<a href="$(HOME)/">https://www.debian.org/</a>, schicken eine E-Mail (auf 
Englisch) an &lt;press@debian.org&gt;, oder kontaktieren das 
Stable-Release-Team (auch auf Englisch) über 
&lt;debian-release@lists.debian.org&gt;.</p>
