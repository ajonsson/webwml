#use wml::debian::translation-check translation="c1e997409ebbfee3f612efc60c0f1e3fe81c7e9c"
<define-tag pagetitle>Canais oficiais de comunicação do Debian</define-tag>
<define-tag release_date>2020-03-16</define-tag>
#use wml::debian::news

<p>
De tempos em tempos, nós do Debian recebemos perguntas sobre nossos canais
oficiais de comunicação e perguntas sobre o status do Debian de quem pode
possuir sites web com nomes semelhantes.
</p>

<p>
O site web principal do Debian <a href="https://www.debian.org">www.debian.org</a>
é nosso meio de comunicação primário. Aqueles(as) que procuram informações sobre
os eventos atuais e o progresso de desenvolvimento na comunidade podem se
interessar pela seção do site web do Debian
<a href="https://www.debian.org/News/">Debian News</a>.

Para anúncios menos formais, temos o blog oficial do Debian
<a href="https://bits.debian.org">Bits do Debian</a>,
e o serviço <a href="https://micronews.debian.org">Debian micronews</a> para
itens de notícias mais curtos.
</p>

<p>
Nosso boletim de notícias oficial
<a href="https://www.debian.org/News/weekly/">Debian Project News</a>
e todos os nossos anúncios oficiais de novidades ou mudanças no projetos são
publicados duas vezes, em nosso site web e enviados para a nossa lista de
discussão oficial
<a href="https://lists.debian.org/debian-announce/">debian-announce</a> ou
<a href="https://lists.debian.org/debian-news/">debian-news</a>.
A postagem nessas listas de e-mail é restrito.
</p>

<p>
Também queremos aproveitar a oportunidade para anunciar como o Projeto Debian,
ou de maneira curta, Debian é estruturado.
</p>

<p>
O Debian tem a estruturada regulada por nossa
<a href="https://www.debian.org/devel/constitution">constituição</a>.
Membros oficiais e delegados(as) estão listados(as) em nossa página de
<a href="https://www.debian.org/intro/organization">estrutura organizacional</a>.
Equipes adicionais estão listadas em nossa página de
<a href="https://wiki.debian.org/Teams">equipes</a>.
</p>

<p>
A lista completa de membros oficiais do Debian pode ser encontrada em nossa
<a href="https://nm.debian.org/members">página de novos membros</a>,
onde nossa associação é gerenciada. A relação completa de colaboradores(as)
Debian pode ser encontrada em nossa página de
<a href="https://contributors.debian.org">colaboradores(as)</a>.
</p>

<p>
Se você tem dúvidas, te convidamos a entrar em contato com a equipe de imprensa
em &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

<h2>Sobre o Debian</h2>
<p>O projeto Debian é uma associação de desenvolvedores(as) de Software Livre
que voluntariamente doam seu tempo e esforço para produzir o sistema operacional
completamente livre Debian.</p>

<h2>Informações para contato</h2>
<p>Para mais informações, por favor visite a página do Debian em
<a href="$(HOME)/">https://www.debian.org/</a> ou envie um e-mail
para &lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
