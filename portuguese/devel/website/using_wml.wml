#use wml::debian::template title="Usando WML"
#use wml::debian::translation-check translation="24a8bbc5bf2fd2fbe025f0baa536bf1126f83723" maintainer="Leonardo Rocha"

<p>WML vem de "web site meta language". Isso significa que o WML
recebe arquivos .wml como entrada, processa o que estiver dentro
deles (pode ser qualquer coisa, de código HTML básico a código Perl!),
e gera o que você quiser que ele gere, por exemplo, .html ou .php.</p>

<p>Não é fácil aprender a partir da documentação do WML. Ela é bem completa,
mas até você entender como ele funciona (e ele é bem poderoso),
é mais fácil aprender com exemplos. Você pode achar os arquivos
de template usados pelo site do Debian úteis. Eles podem ser encontrados em
<code><a href="https://salsa.debian.org/webmaster-team/webwml/tree/master/english/template/debian">\
webwml/english/template/debian/</a></code>.</p>

<p>Pressupõe-se que você tenha WML instalado no seu computador.
Ele está disponível como um
<a href="https://packages.debian.org/wml">pacote Debian</a>.


<h2>Editando fontes WML</h2>

<p>Algo que todos os arquivos .wml terão é uma ou mais linhas
<code>#use</code> no começo. Você não pode alterar ou traduzir sua
sintaxe, apenas as strings entre aspas como aquelas depois de
<code>title=</code>, o que alteraria o elemento &lt;title&gt;
nos arquivos de saída.</p>

<p>Além das linhas de cabeçalho, a maioria das nossas páginas .wml
contém HTML simples. Se você encontrar tags como &lt;define-tag&gt; ou
&lt;: ... :&gt;, seja cuidadoso, pois estas delimitam códigos que são
processados em um dos passos especiais do WML. Veja abaixo para mais
informações.</p>


<h2>Construindo páginas web do Debian</h2>

<p>Simplesmente digite <kbd>make</kbd> em webwml/&lt;lang&gt;. Nós
configuramos makefiles que invocam <kbd>wml</kbd> com todos os argumentos
certos.</p>

<p>Se você executar um <kbd>make install</kbd>, os arquivos html serão
construídos e colocados no diretório <kbd>../../www/</kbd>.</p>


<h2>Recursos extras do WML que nós usamos</h2>

<p>Um dos recursos do WML que nós usamos extensivamente é o uso de Perl.
Lembre-se, estas páginas não são dinâmicas. O Perl é usado no momento que
as páginas são geradas para fazer qualquer coisa que você queira. Dois
bons exemplos de como nós estamos usando Perl nas páginas é para criar
uma lista das notícias mais recentes para a página principal e
gerar links para as traduções no final da página.

# TODO: add the basic stuff from webwml/english/po/README here

<p>Para reconstruir os templates de nosso site web, a versão &gt;= 2.0.6
do wml é necessária. Para reconstruir os templates gettext para traduções
não-inglesas, o mp4h &gt;= 1.3.0 é necessário.</p>


<h2>Questões específicas com o WML</h2>

<p>Idiomas multi-byte podem precisar de pré ou pós processamento especial
para os arquivos .wml para lidar corretamente com o conjunto de caracteres.
Isto pode ser feito alterando as variáveis <kbd>WMLPROLOG</kbd> e
<kbd>WMLEPILOG</kbd> em <kbd>webwml/&lt;lang&gt;/Make.lang</kbd>
apropriadamente. Dependendo de como o seu programa <kbd>WMLEPILOG</kbd>
funciona, você pode precisar alterar o valor de <kbd>WMLOUTFILE</kbd>.
<br>
Veja as traduções japonesas ou chinesas para um exemplo.
</p>
