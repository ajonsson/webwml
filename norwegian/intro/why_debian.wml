#use wml::debian::template title="Grunner til å velge Debian"
#include "$(ENGLISHDIR)/releases/info"
#use wml::debian::translation-check translation="7141cf5f25b03b1aa3c02c1afe2a5ca8cab2f672" maintainer="Hans F. Nordhaug"
# Oversatt til norsk av Tor Slettnes (tor@slett.net)
# Oppdatert av Hans F. Nordhaug <hansfn@gmail.com>, 2008-2021

<p>Det er mange grunner til at brukere skal velge Debian som sitt operativsystem.</p>

<h1>Viktigste grunner</h1>

# try to list reasons for enn users first

<dl>
  <dt><strong>Debian er fri programvare.</strong></dt>
  <dd>
    Debian er laget med fri og åpen programvare, og vil alltid være 100%
    <a href="free">fri</a>. Fri for alle til å bruke, endre og distribuere.
    Dette er vårt hovedløfte til <a href="../users">våre brukere</a>. Det er 
    også uten kostnad.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er et stabilt og sikkert Linux-basert operativsystem.</strong></dt>
  <dd>
    Debian er et operativsystem for et bredt utvalg av enheter, herunder bærbare og 
    stasjonære datamaskiner, og servere. Siden 1993 har brukerne vært glade for 
    stabiliteten og påliteligheten. Vi leverer fornuftige standardinnstillinger 
    for alle pakkene. Debian-utviklerne tilbyr sikkerhetsoppdateringer 
    alle pakkene i løpet av deres levetid, hvor det er mulig.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har omfattende støtte av maskinvare.</strong></dt>
  <dd>
    Det meste av maskinvare er allerede støttet av Linux-kjernen. Proprietære 
    drivere til maskinvare er tilgjengelig når den frie programvare ikke er 
    tilstrekkelig.
  </dd>
</dl>

<dl>
  <dt><strong>Debian gir problemfrie oppgraderinger.</strong></dt>
  <dd>
    Debian er velkjent for sine problemfrie oppgraderinger innenfor en 
    utgavesyklus, men også til den neste større utgave.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er utspringet og grunnlaget for mange andre distribusjoner.</strong></dt>
  <dd>
    Mange av de mest populære Linux-distribusjonene, slik som Ubuntu, Knoppix, 
    PureOS, SteamOS og Tails, har valgt Debian som grunnlag for deres programvare.
    Debian stiller alle verktøy til rådighet, så alle kan supplere 
    programvarepakkerne fra Debians arkiv med deres egne pakker, som oppfyller deres 
    behov.
  </dd>
</dl>

<dl>
  <dt><strong>Debian-projektet er et fellesskap.</strong></dt>
  <dd>
    Debian er ikke bare et Linux-operativsystem. Programvaren fremstilles i samarbeid 
    med hundrevis av frivillige over hele verden. Du kan delta i Debians 
    fellesskap selv om du ikke er programmerer eller systemadministrator. 
    Debian styres av sitt fellesskap og av konsensus, og har en 
    <a href="../devel/constitution">demokratisk styreform</a>. Da alle 
    Debian-utviklere har de samme rettigheter kan projektet ikke kontrolleres 
    av en enkelt virksomhet. Vi har utviklere i flere enn 60 land, og vårt 
    installasjonsprogram er oversatt til flere enn 80 språk.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har mange installasjonsmetoder.</strong></dt>
  <dd>
    Sluttbrukerne kan bruke vår 
    <a href="https://cdimage.debian.org/images/release/current-live/amd64/iso-hybrid/">Live-CD</a>,
    herunder den letanvendelige Calamares-installer, som krever veldig lite 
    inntastninger eller forkunnskap. Mer erfarne brukere kan anvende våre 
    unike, komplette installasjonsprogram, mens eksperter kan fininnstille 
    installasjonen eller til og med bruke et automatisert verktøy til 
    nettverksinstallasjon.
  </dd>
</dl>

<br>

<h1>Bedriftsmiljø</h1>

<p>
  Hvis du har behov for Debian i et professionelt miljø, er det ytterligere fordeler:
</p>

<dl>
  <dt><strong>Debian er pålitelig.</strong></dt>
  <dd>
    Debian beviser dagligt sin pålitelighet i tusenvis av scenarier i den 
    virkelige verden, fra enkeltbrukeres bærbare datamaskiner til 
    superdatamaskoner, børser og bilindustrien. Debian er også populær i den 
    akademiske verden, i videnskapelige miljøer og i den offentlige sektor.
  </dd>
</dl>

<dl>
  <dt><strong>Debian har mange eksperter.</strong></dt>
  <dd>
    Våre pakkevedlikeholdere tar seg ikke bare av Debian-pakking og 
    innarbeidelse av nye oppstrømsversjoner. Ofte er de eksperter i 
    oppstrømsprogramvaren, og bidrar direkte til oppstrømsutviklingen.  
    Noen ganger er de også en del av oppstrøm.
  </dd>
</dl>

<dl>
  <dt><strong>Debian er sikker.</strong></dt>
  <dd>
    Debian har sikkerhetsstøtte av sine stabile utgivelser. Mange andre 
    distribusjoner og sikkerhetsefterforskere er afhængige av Debians 
    sikkerhetssporingsverktøy.
  </dd>
</dl>

<dl>
  <dt><strong>Long Term Support.</strong></dt>
  <dd>
    Det er gratis <a href="https://wiki.debian.org/LTS">Long Term Support</a>
    (LTS/langtidsstøtte). Det gir deg utvidet støtte av den 
    stabile utgaven i fem år eller mer. Dessuten er det også et
    <a href="https://wiki.debian.org/LTS/Extended">Extended LTS</a>-initiativ, 
    som utvider støtte av et begrenset antall pakker i mer enn fem år.
  </dd>
</dl>

<dl>
  <dt><strong>Cloud-filbilder.</strong></dt>
  <dd>
    Offisielle cloud-filbilder er tilgjengelige for alle de største platformene.  
    Vi tilbyder også verktøy og konfigurasjon så du kan bygge ditt eget 
    skreddersydde cloud-filbilde. Du kan også bruke Debian i virtuelle 
    maskiner på skrivebordet eller i en container.
  </dd>
</dl>

<br>

<h1>Utviklere</h1>

<p>Debian er mye brukt blant alle former for programvare- og maskinvareutviklere.</p>

<dl>
  <dt><strong>Offentligt tilgjengelig feilsporingssystem.</strong></dt>
  <dd>
    Vårt <a href="../Bugs">feilsporingssystem</a> (BTS) er offentlig 
    tilgjengelig for alle gjennom en nettleser.  Vi skjuler ikke våre 
    programvarefeil, og du kan lett sende inn nye feilrapporter.
  </dd>
</dl>

<dl>
  <dt><strong>IoT og innebygde enheter.</strong></dt>
  <dd>
    Vi støtter et bredt utvalg av enheter, som eksempelvis Raspberry Pi, 
    forskjellige varianter av QNAP, mobile enheter, hjemmeruter og mange 
    Single Board Computers (SBC).
  </dd>
</dl>

<dl>
  <dt><strong>Mange maskinvarearkitekturer.</strong></dt>
  <dd>
    Det er støtte for en <a href="../ports">lang liste</a> med 
    CPU-arkitekturer, herunder amd64, i386, mange versioner av ARM og MIPS, 
    POWER7, POWER8, IBM System z og RISC-V. Debian er også tilgjengelig for 
    eldre og spesifikke nichearkitekturer.
  </dd>
</dl>

<dl>
  <dt><strong>Enormt antall tilgjengelige programvarepakker.</strong></dt>
  <dd>
    Debian har det største antallet av pakker tilgjengelige for installasjon
    (for øyeblikket <packages_in_stable>). Våre pakker bruker deb-formatet, 
    som er velkjent for sin høye kvalitet.
  </dd>
</dl>

<dl>
  <dt><strong>Forskjellige tilgjengelige utgivelser.</strong></dt>
  <dd>
    Ut over vår stabile utgave, kan du også få de nyeste programvareversjoner ved 
    å bruke utgavene testing eller unstable.
  </dd>
</dl>

<dl>
  <dt><strong>Høy kvalitet ved hjelp av utviklerverktøy og fremgangsmåter.</strong></dt>
  <dd>
    Flere utviklerverktøy hjelper med at holde kvaliteten på et høyt nivå, 
    og våre <a href="../doc/debian-policy/">fremgangsmåter</a> definerer de 
    tekniske krav, som alle pakker skal oppfylle for å kunne bli tatt med i 
    distribusjonen.  Vår continuous integration kjører programvaren autopkgtest, 
    piuparts er våre testverktøy til installasjon, oppgradering og fjerning, 
    og lintian er et omfattende verktøy til å kontrollere Debian-pakker med.
  </dd>
</dl>

<br>

<h1>Hva våre brukere sier</h1>

<ul style="line-height: 3em">
  <li>
    <q><strong>
      For meg har Debian det perfekte nivået med brukervennlighet og stabilitet.
      Jeg har brukt mange forskjellig distribusjoner gjennom årene, men Debian
      er den eneste som bare virker.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Bunnsolid. Massevis av pakker. Glimrende fellesskap.
    </strong></q>
  </li>

  <li>
    <q><strong>
      Debian er for meg et symbol på stabilitet og brukervennlighet.
    </strong></q>
  </li>
</ul>

