#use wml::debian::template title="Debian &ldquo;buster&rdquo;-utgivelsesinformasjon"
#include "$(ENGLISHDIR)/releases/info"
#include "$(ENGLISHDIR)/releases/buster/release.data"
#include "$(ENGLISHDIR)/releases/arches.data"
#use wml::debian::translation-check translation="6cb92bf51af80b4a4dc15d92247adf5ba8fe9619" maintainer="Hans F. Nordhaug"
# Oversatt til norsk av Hans F. Nordhaug <hansfn@gmail.com>

<if-stable-release release="buster">
<p>
  Debian <current_release_buster> ble utgitt <a href="$(HOME)/News/<current_release_newsurl_buster/>"><current_release_date_buster></a>. 
  <ifneq "10.0" "<current_release>"
  "Debian 10.0 ble først utgitt <:=spokendate('2019-07-06'):>."
  />
  Utgivelsen inkluderte mange store endringer beskrevet i vår
  <a href="$(HOME)/News/2019/20190706">pressemelding</a> og i
  <a href="releasenotes">utgivelsesmerknadene</a>.
</p>

#<p><strong>Debian 10 has been superseded by
#<a href="../bullseye/">Debian 11 (<q>bullseye</q>)</a>.
#Security updates have been discontinued as of <:=spokendate('xxxx-xx-xx'):>.
#</strong></p>

### This paragraph is orientative, please review before publishing!
#<p><strong>However, buster benefits from Long Term Support (LTS) until
#the end of xxxxx 20xx. The LTS is limited to i386, amd64, armel, armhf and arm64.
#All other architectures are no longer supported in buster.
#For more information, please refer to the <a
#href="https://wiki.debian.org/LTS">LTS section of the Debian Wiki</a>.
#</strong></p>

<p>
  For å få tak og installere Debian, se siden med
  <a href="debian-installer/">installasjonsinformasjon</a> og 
  <a href="installmanual">installasjonshåndboken</a>. 
  For å oppgradere fra eldre Debian utgaver, se instruksjonene i 
  <a href="releasenotes">utgivelsesmerknadene</a>.
</p>

### Activate the following when LTS period starts.
#<p>Følgende datamaskinarkitekturer er støttet i LTS-perioden:</p>
#
#<ul>
#<:
#foreach $arch (@archeslts) {
#	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
#}
#:>
#</ul>

<p>Følgende datamaskinarkitekturer er støttet i denne utgaven:</p>
# <p>Da buster ble utgitt var følgende datamaskinarkitekturer støttet:</p> ### Use this line when LTS starts.

<ul>
<:
foreach $arch (@arches) {
	print "<li><a href=\"$(HOME)/ports/$arch/\">$arches{$arch}</a>\n";
}
:>
</ul>

<p>
  I motsetning til våre ønsker kan det være problemer i denne utgaven selvom
  den er erklært <em>stabil</em>. Vi har lagd <a href="errata">en list med de
  viktigste kjente problemene</a>, og du kan alltid 
  <a href="reportingbugs">rapportere andre problemer</a> til oss.
</p>

<p>
  Sist, men ikke minst, har vi en liste med <a href="credits">folk som har
  sørget for at denne utgaven ble utgitt</a>.
</p>
</if-stable-release>

<if-stable-release release="jessie">
<p>Ingen informasjon tilgjengelig ennå.</p>
</if-stable-release>

<if-stable-release release="stretch">

<p>Kodenavnet på den neste store Debian-utgivelse etter 
<a href="../stretch/">stretch</a> er <q>buster</q>.</p>

<p>Denne utgaven begynte som en kopi av stretch og er for øyeblikket i en fase
kalt <q><a href="$(DOC)/manuals/debian-faq/ftparchives#testing">testing</a></q>. 
Det betyr at ting ikke burde slutte å virke på en så alvorlig måte 
som i den ustabile eller eksperimentelle distribusjonen, fordi pakkene kun 
får lov til å bli med i distribusjonen etter et gitt tidsperiode har passert, 
og når der ikke er registrert utgivelseskritiske feil på dem.</p>

<p>Merk deg at sikkerhetsoppdateringer til <q>testing</q>-distribusjonen 
ennå <strong>ikke</strong> håndteres av sikkerhetsteamet. Derfor mottar 
<q>testing</q> <strong>ikke</strong> jevnlige sikkerhetsoppdateringer.  
# For flere oplysninger, se 
# <a href="https://lists.debian.org/debian-testing-security-announce/2008/12/msg00019.html">\
# annonceringen</a> fra Testing Security Team.
Du oppfordres til å endre dine sources.list-linjer fra testing til stretch inntil 
videre, hvis du har bruk for sikkerhetsbrukerstøtte. Se også punktet i 
<a href="$(HOME)/security/faq#testing">sikkerhetsteamets OSS</a> vedrørende
<q>testing</q>-distribusjonen.</p>

<p>Et <a href="releasenotes">utkast til utgivelsesmerknadene</a> kan være tilgjengelig. 
Sjekk også <a href="https://bugs.debian.org/release-notes">de foreslåtte
tilleggene til utgivelsesmerknadene</a>.</p>

<p>For installasjonsbilder og dokumentasjon om hvordan man installerer 
<q>testing</q>, se <a href="$(HOME)/devel/debian-installer/">siden om 
Debian-Installer</a>.</p>

<p>For å få flere opplysninger om hvordan <q>testing</q>-distribusjonen fungerer, se
<a href="$(HOME)/devel/testing">utviklernes opplysninger om den</a>.</p>

<p>Folk spør ofte om det fins en indikator for hvor langt vi er kommet med
utgaven. Dessverre fins det ikke en slik indikator, men vi kan vise til flere 
steder som beskriver ting som må behandles før utgivelsen kan skje:</p>

<ul>
    <li><a href="https://release.debian.org/">Generisk utgivelses-statusside</a></li>
    <li><a href="https://bugs.debian.org/release-critical/">Utgivelseskritiske feil</a></li>
    <li><a href="https://udd.debian.org/bugs.cgi?base=only&amp;rc=1">Feil i grunnsystemet</a></li>
    <li><a href="https://udd.debian.org/bugs.cgi?standard=only&amp;rc=1">Feil i standard- og task-pakker</a></li>
</ul>

<p>I tillegg sender den utgivelsesansvarlige generelle statusrapporter på 
<a href="https://lists.debian.org/debian-devel-announce/">postlisten 
debian-devel-announce</a>.</p>

</if-stable-release>
