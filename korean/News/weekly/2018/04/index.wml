#use wml::debian::projectnews::header PUBDATE="2018-12-11" SUMMARY="Welcome to the DPN, Debian 9.6 released, Package Salvaging, Reproducible Builds joins the Software Conservancy, Rust available on 14 Architectures, CTTE decision, Upcoming freeze timeline, Bits from the Anti-Harassment Team, Buster PSP coming up, Reports, Calls for Help, More than just code, Quick Links from Debian Social Media"
#use wml::debian::acronyms
#use wml::debian::translation-check translation="698c9bffdac565fc3f105ac6d5d81e7c5bf9eeb2" maintainer="Sebul" mindelta="-1"
# 주의: 불완전한 번역. 번역을 마친 다음 위의 'mindelta="-1"'을 지우십시오.

# $Rev$
# Status: [content-frozen]


## substitute XXX with the number (expressed in letter) of the issue.
## please note that the var issue is not automagically localized, so
## translators need to put it directly in their language!
## example: <intro issue="fourth" />

## Use &#35; to escape # for IRC channels
## Use &#39 for single quotes

<intro issue="fourth" />
<toc-display/>

<toc-add-entry name="newdpn">데비안 프로젝트 뉴스에 오신 것을 환영합니다!</toc-add-entry>

<p>We hope that you enjoy this edition of the DPN.</p>

<p>For other news, please read the official Debian blog
<a href="https://bits.debian.org">Bits from Debian</a>, and follow
<a href="https://micronews.debian.org">https://micronews.debian.org</a> which
feeds (via RSS) the @debian profile on several social networks too.</p>
<p>At the end of this Project News we've added a <b>Quick Links</b> section which
links to a selection of the posts made through our other media streams.</p>

<p>Debian's Security Team releases current advisories on a daily basis
(<a href="$(HOME)/security/2018/">Security Advisories 2018</a>). Please
read them carefully and subscribe to the <a href="https://lists.debian.org/debian-security-announce/">security
mailing list</a>.</p>

<toc-add-entry name="internal">내부 뉴스/해프닝</toc-add-entry>

<p><b>데비안 9 업데이트: 9.6 릴리스</b></p>
<p>The Debian project <a href="https://www.debian.org/News/2018/20181110">announced</a>
the sixth update of its stable distribution Debian 9 (codename <q>Stretch</q>) on 10 November 2018 
to point release 9.6.</p>

#<p>The Debian project also <a href="https://www.debian.org/News/2018/2018MMDD">announced</a>
#the YYYY update of its oldstable distribution Debian 8 (codename <q>Jessie</q>) on DD Month 2018
#to point release 8.Y.</p>

<p>This point release added corrections for security issues along with a few
adjustments for serious problems. Security advisories have already been published
separately and are referenced where available.

Upgrading an existing installation to either revision can be achieved
by pointing the package management system at one of Debian's many HTTP mirrors.
A comprehensive list of mirrors is available at: <a href="https://www.debian.org/mirror/list">https://www.debian.org/mirror/list</a>
</p>



<p><b>Package Salvaging</b></p>

<p>Tobias Frost <a href="https://lists.debian.org/debian-devel-announce/2018/09/msg00003.html">announced</a>
a new addition to the Debian Developer's Reference on Package Salvaging.
<a href="https://www.debian.org/doc/manuals/developers-reference/ch05.en.html#package-salvaging">Package Salvaging</a>
allows packages not officially orphaned or abandoned to be maintained by other
developers or new contributors after some eligibility factors are addressed.</p>

<p>The process differs from MIA handling of packages in that it does allow for negelected
or forgotten packages to be brought back into the fold. There is a set of <a href="https://wiki.debian.org/PackageSalvaging">guidelines</a>
available which outline the phases of the process, along with additional
information and FAQs on the Debian Wiki.</p>


<p><b>Reproducible Builds joins the Software Conservancy!</b></p>

<p><a href="https://reproducible-builds.org/">Reproducible Builds</a> has joined the <a href="https://sfconservancy.org/about/">Software Freedom Conservancy</a>, a 501(c)(3) not-for-profit
organisation that helps promote, develop and defend Free, Libre, and Open Source
Software (FLOSS) projects.  Through the SFC, member projects may receive donations
earmarked for the benefit of a specific FLOSS project.</p>

<p>The Reproducible Builds project, which <a href="https://wiki.debian.org/ReproducibleBuilds/History">began as a project</a> within the Debian community,
is also critical to the Conservancy’s own compliance work: A build that cannot be
verified may contain code that triggers different license compliance responsibilities
than the recipient is expecting.</p>

<p>As Reproducible Builds joins Conservancy, it is also receiving a donation of US$300,000
from the <a href="https://handshake.org/">Handshake Foundation</a> which will propel the project’s efforts to ensure the
future health and usability of free software.</p>

<p><b>Rust available on 14 Debian Architectures</b></p>

<p>John Paul Adrian Glaubitz announced and thanked the many contributors who helped
to get <a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00000.html">Rust available on 14 Debian architectures</a>.
The <a href="https://buildd.debian.org/status/package.php?p=rustc&amp;suite=unstable">newest</a>
supported architectures are: mips, mips64el, mipsel, and  powerpcspe.</p>
<p>This work is the result of the
combined effort of many talented people, and work on LLVM upstream which
fixed many many bugs in the MIPSand PowerPC backends as well as adding support
for the PowerPCSPE sub-target.</p>

<p><b>Documenting copyright holders in debian/copyright</b></p>


<p>The FTP team has issued <a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00004.html">clarifications</a>
in regard to copyright attribution in debian/copyright; some of the main points:</p>

<ul>
<li>Unless a license explicitly states that copyright attributions only apply to
source distributions, they apply as well for the source and binary. The copyright
must be documented in debian/copyright for license compliance reasons.</li>

<li>Be mindful of <a href="https://www.debian.org/doc/debian-policy/ch-archive.html#copyright-considerations">2.3 Copyright considerations</a>: Every package must be accompanied by
a verbatim copy of its copyright information and distribution license in the file
/usr/share/doc/package/copyright.</li>

<li>On rare occasion the FTP masters have determined that full copyright attribution
is both not feasible and, given the nature of the package, that an appropriate
copyright notice does not need to list all copyright holders; in such
cases this tolerance should not be assumed to apply to other packages.</li>
</ul>

<p>The FTP team affirms that documenting copyright holders in debian/copyright is
a good idea.</p>

<p><b>CTTE decision on vendor-specific patch series</b></p>

<p>The technical committee <a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00004.html">passed a resolution</a> on whether to allow the use of
vendor-specific patch series in the Debian archive, in summary:</p>

<p>The Committee recognises a need for packages to behave differently when built
on different distributions, but this should be done by using differing source
packages, or as part of the build process using current and future practices
such as patches with conditional behaviour or patching of files during the build
rather than at source unpacking time.</p>

<p>As this feature is used by several packages today, there is the need for a
reasonable transition period. However, they will be considered buggy from
when this resolution is accepted, but will not be considered severe enough
to warrant immediate removal from Debian.</p>

<p>After Buster is released, the presence of a vendor-specific patch series
will be a violation of a MUST directive in Debian policy.</p>

<p>The Committee therefore resolves:
Any use of dpkg's vendor-specific patch series feature is a bug for packages
in the Debian archive (including contrib and non-free).</p>

<p>After Buster is released, use of the vendor-specific patch series feature is
forbidden in the Debian archive.</p>

<p>For additional information and the original discussion please see <a href="https://bugs.debian.org/904302">Bug #904302</a>.</p>

<p><b>Release Team: Upcoming freeze timeline, ways to help</b></p>

<p>The Release Team is <a href="https://lists.debian.org/debian-devel-announce/2018/09/msg00004.html">preparing</a> for the initial phase of the buster freeze.</p>

<p>Developers are reminded to follow up on their plans and evaluate realistic
timelines to accomplish for changes and inclusion into buster.</p>

<p>Changes can be staged in experimental, to avoid disruption. Keep in mind
that other volunteers may not have the same capacity to work on your goals.
Any unfixed bugs are suggested to be fixed via <a href="https://wiki.debian.org/NonMaintainerUpload">NMU</a> now rather than later.</p>

<p>The official freeze time table for buster is:</p>
<ul>
 <li>2019-01-12 - Transition freeze</li>
 <li>2019-02-12 - Soft-freeze</li>
 <li>2019-03-12 - Full-freeze</li></ul>

<p>Please consult the buster <a href="https://release.debian.org/buster/freeze_policy.html">freeze policy</a> and timeline for detailed information about the different types of
freezes and what they mean for you.</p>

<p>If you would like to help us to get buster out on time and are able to
help fix RC bugs in testing prior to the transition freeze, you can do that now
by looking at the <a href="https://udd.debian.org/bugs/">list of RC bugs</a> or joining the &#35;debian-bugs irc channel on irc.oftc.net.</p>

<p><b>Bits from the Debian Anti-Harassment Team</b></p>


<p>The <a href="https://wiki.debian.org/AntiHarassment">Debian Anti-Harassment Team</a> is the point of contact for any community
member who would like to help create a more welcoming and respectful
environment in Debian, and is also the point of contact for reports or concerns
about inappropriate behaviour or abuse. The team will send out small but
regular reports to the community.</p>

<p>Should you see interactions that you consider deserve attention, please let
us know. Please do not wait until a problem becomes too big; we can assist
as friendly de-escalators or as mediators. Members may also forward
information for which no action is to be taken, but kept on file should a
problem escalate some time in the future.</p>

<p>The team may be contacted at <a href="mailto:antiharassment@debian.org">antiharassment@debian.org</a>.</p> 

<p>Some highlights of our recent activity:</p> 

<p>One request for intervention on a dispute about a package deemed offensive,
we issued our recommendation: Bug <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=907199">#907199</a></p>

<p>We had a request about removing messages from mailing list and responded.</p>

<p>Several attendees at DebConf 18 served as a local anti-harassment team
that handled disputes between attendees and a possible Code of Conduct violation,
mediating in minor issues, and offering advice during the conference.</p>

<p>A general reminder about the CoC was sent via micronews several times during
DebConf18.</p>

<p>We have been involved in the discussions about the photo policy for DebConf,
and we plan to make a proposal soon.</p>

<p><b>New Outreachy intern</b></p>

<p>Debian welcomes <a href="https://bits.debian.org/2018/11/welcome-outreachy-intern-2018-2019.html">Anastasia Tsikoza as our newest Outreachy intern</a>. The Outreachy
program provides internships for people from groups traditionally
underrepresented in technology. Anastasia mentored by Paul Wise and Raju Devidas,
will work on Improving the integration of Debian derivatives with the Debian
infrastructure and the community.</p>



<p><b>Misc Developer News</b></p>

<p>Paul Wise <a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00002.html">posted</a> the most recent issue of Misc Developer News &#35;46, highlights
include:</p>

<p>debhelper compat level 12 is open for beta testing and is expected to become stable in Debian buster.</p>

<p>A new port for RISC-V flavour <q>riscv64</q> (64-bits little-endian) is now available in Debian Ports.</p>

<p>With the recent release of debcargo 2.0.0 to crates.io, Debian packages can be created from your favorite Rust crate and uploaded to the Debian archive.</p>

<p>devscripts 2.18.5 has been released and brings some new uscan features such as verifying signed tags in git and auto value for dversionmangle.</p>

<p>Chris Lamb called for more volunteers for the FTP Team.</p>


<toc-add-entry name="events">Events: Upcoming and Reports</toc-add-entry>

<p><b>Upcoming events</b></p>

<p><b>MiniDebConf Marseille 2019</b></p>
<p>
A miniDebConf will take place in Marseille (France) from 25 to 26 May, with
two days of talks, lightning talks, keysigning party, lunch, and ... beer event
Read the <a href="https://france.debian.net/pipermail/minidebconf/2018-November/000159.html">announcement</a>
and visit the <a href="https://wiki.debian.org/DebianEvents/fr/2019/Marseille">wiki page of the event</a>
where you can get all the details, register for the event, and help in the
organisation.</p>

<p><b>Buster (bug) Squashing Parties!</b></p>

<p>There are many Bug Squashing Parties coming up on our calendars that are
focusing efforts on addressing and fixing release critical bugs that will delay
the release of Debian 10 (buster). BSPs are open to everyone who wants and is
able to get involved. Come on by and help us to make this release a success!</p>

<p><b>Netherlands, Venlo, 12 January to 13 January 2019</b></p>

<p>Hosted at Transceptor Technology and insign.it.</p>

<p>Feel welcome if you want to contribute to Debian, whatever your
experience level. You don't need to be an existing Debian contributor.
Just trying to reproduce a bug and documenting your experience is
already useful.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00005.html">Announcement for Venlo BSP</a></p>
<p><a href="https://wiki.debian.org/BSP/2019/01/nl/Venlo"> BSP Wiki for Venlo</a></p>


<p><b>Canada, Montreal, 19 January to 20 January 2019</b></p>

<p>Hosted at Eastern Bloc, Montreal, Canada.</p>

<p>Unlike the one we organised for the Stretch release, this BSP will be over a
whole weekend, so hopefully folks from other provinces in Canada and from the
USA can come. </p>
<p>You can register on the wiki page where you will find information
regarding transport, accommodation, food and other useful things. Expenses to
attend this BSP should be sponsored by the Debian Project</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/12/msg00000.html">Announcement for Montreal BSP</a></p>
<p><a href="https://wiki.debian.org/BSP/2019/01/ca/Montreal"> BSP Wiki for Montreal</a></p>


#<p>
#venue yet unknon
#There will be a <a href="https://wiki.debian.org/BSP/2019/01/us/Austin">Bug
#Squashing Party</a> on 25&ndash;27 January 2019, held in Austin, Texas, USA.
#</p>

<p><b>Germany, Bonn, 22 to 24 February 2019</b></p>

<p>Tarent solutions GmbH, Rochussstr. 2, 53123 Bonn, Germany</p>

<p>The BSP is scheduled right between the soft freeze and the full freeze, thus
giving a perfect opportunity for a really efficient and concentrated RC bug
squashing sprint.</p>

<p>The venue offers enough room for up to 20 people, separate rooms for those
who want to hack in a smaller team, and also room for socialising.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00001.html">Announcement for Bonn BSP</a></p>
<p><a href="https://wiki.debian.org/BSP/2019/02/de/Bonn">BSP Wiki for Bonn</a></p>


<p><b>Austria, Salzburg  05 April to 07 April 2019</b></p>

<p>The offices of Conova Communications GmbH [CONOVA], located close to
Salzburg Airport W.A. Mozart.</p>

<p>We are happy to invite you to the 6th Debian Bug Squashing Party in
Salzburg, Austria.</p> 

<p>A short registration on the wiki page [BSPSBG] is required to ease the
organisation of the event. On the same page you will find information
regarding transport, (sponsored) accommodation and other useful things.</p>

<p><a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00006.html">Announcement for Salzburg BSP</a></p>
<p><a href="https://wiki.debian.org/BSP/2019/04/Salzburg">BSP Wiki for Salzburg</a></p>


<toc-add-entry name="reports">Reports</toc-add-entry>
## It's easier to link to the monthly reports for the LTS section and the RB links rather than
# summarize each report or number of packages. Feel free to input this information if you need to fill
# the issue out
#
<p><b>LTS Freexian Monthly Reports</b></p>

<p>Freexian issues <a href="https://raphaelhertzog.com/tag/Freexian+LTS/">monthly reports</a>
about the work of paid contributors to Debian Long Term Support.
</p>

<p><b>Reproducible Builds status update</b></p>

<p>Follow the <a
href="https://reproducible-builds.org/blog/">Reproducible
Builds blog</a> to get the weekly reports on their work in the <q>Buster</q> cycle.
</p>


<toc-add-entry name="help">Help needed</toc-add-entry>

##<p><b>Teams needing help</b></p>
## Teams needing help
## $Link to the email from $Team requesting help


<p><b>Packages needing help</b></p>

## link= link to the mail report from wnpp@debian.org to debian-devel ML
## orphaned= number of packages orphaned according to $link
## rfa= number of packages up for adoption according to $link

<wnpp link="https://lists.debian.org/debian-devel/2018/11/msg00733.html"
	orphaned="1311"
	rfa="157" />

<p><b>새로운 버그</b></p>

## check https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer and add outstanding + forwarded + pending upload
<p>
Debian has a <q>newcomer</q> bug tag, used to indicate bugs which are suitable for new
contributors to use as an entry point to working on specific packages.

There are currently <a href="https://bugs.debian.org/cgi-bin/pkgreport.cgi?tag=newcomer">212</a>
bugs available tagged <q>newcomer</q>.
</p>

<toc-add-entry name="morethancode">More than just code</toc-add-entry>

<p>Carl Chenet opined, <a href="https://carlchenet.com/you-think-the-visual-studio-code-binary-you-use-is-a-free-software-think-again/">You Think the Visual Studio Code binary you use is a Free
Software? Think again.</a> He points out some of the licensing practices used in
regard to the MIT license, a permissive Free Software license.</p>

<p>Elana Hashman shares information on her <a href="https://hashman.ca/pygotham-2018/">PyGotham 2018 Talk Resources</a> for
a talk called "The Black Magic of Python Wheels", based on 2 years of work on
auditwheel and the manylinux platform.</p>

<p>Benjamin Mako Hill talks about <a href="https://mako.cc/copyrighteous/what-we-lose-when-we-move-from-social-to-market-exchange">What we lose when we move from social to market exchange</a>,
on the topic of exchanging money in return for something vs. the exchange of hospitality.</p>

<p>Molly de Blanc shared how she came to run a <a href="http://deblanc.net/blog/2018/11/24/conservancy-match/">Conservancy Match</a> donation program  
for the benefit of Software Freedom Conservancy. </p>


<toc-add-entry name="code">Code, coders, and contributors</toc-add-entry>
<p><b>New Package Maintainers since 19 August</b></p>
##Run the ./new-package-maintainers script locally
##./new-package-maintainers YYYMMDD

<p>Please welcome: Pedro Loami Barbosa dos Santos, Alexandros Afentoulis,
David Kunz, Helen Koike, Andreas Schwarz, Miriam Retka, GreaterFire, Birger
Schacht, Simon Spöhel, Guillaume Pernot, Joachim Nilsson, Mujeeb Rahman K, Timo
Röhling, Hashem Nasarat, Christophe Courtois, Matheus Faria, Oliver Dechant,
Johan Fleury, Gabriel Filion, Baptiste Beauplat, Bastian Germann, Markus
Wurzenberger, Jeremy Finzel, Mangesh Divate, Jonas Schäfer, Julian Rüth,
Scarlett Moore, Tiago Stürmer Daitx, Tommi Höynälänmaa, Romuald Brunet,
Gerardo Ballabio, Stewart Ferguson, Julian Schauder, Chen-Ying Kuo, Denis
Danilov, David Lamparter, and Kienan Stewart.</p>

<p><b>New Debian Maintainers</b></p>
##Run the ./dd-dm-from-keyring.pl script locally for new DM and DD
##./dd-dm-from-keyring.pl YYYMMDD

<p>Please welcome: Sagar Ippalpalli, Kurt Kremitzki, Michal Arbet, Peter Wienemann,
Alexis Bienvenüe, and Gard Spreemann.</p>

<p><b>새 데비안 개발자</b></p>
<p>Please welcome: Joseph Herlant, Aurélien Couderc, Dylan Aïssi, Kunal Mehta,
Ming-ting Yao Wei, Nicolas Braud-Santoni, Pierre-Elliott Bécue, Stephen Gelman,
Daniel Echeverry, and Dmitry Bogatov.</p>


<p><b>기여자</b></p>

## Visit the link below and pull the information manually.

<p>
1603 people and 19 teams are currently listed on the
<a href="https://contributors.debian.org/">Debian Contributors</a> page for 2018.
</p>

<p><b>Statistics</b></p>
##Pull this information from sources.d.o
##https://sources.debian.org/stats/sid/
##https://sources.debian.org/stats/buster/
<p><b><em>buster</em></b></p>
<ul style="list-style-type:none">
<li>Source files: 11,885,550</li>
<li>Source packages: 28,697</li>
<li>Disk usage: 252,791,492 kB</li>
<li>Ctags: 17,452,645</li>
<li>Source lines of code: 1,044,492,396</li>
</ul>

<p><b><em>sid</em></b></p>
<ul>
<li>Source files: 20,019,227</li>
<li>Source packages: 33,533</li>
<li>Disk usage: 381,351,424 kB</li>
<li>Ctags: 42,219,156</li>
<li>Source lines of code: 1,759,157,606</li>
</ul>


<p><b>토론</b></p>

<p>Debian user Aurélien Couderc asked about <a href="https://lists.debian.org/debian-devel/2018/09/msg00220.html">Bumping an epoch and reusing a package name</a>,
which led to a discussion about requesting upstream changes for Debian internal
policies and the effect on users. Several alternatives are mentioned in the 
discussion as well pitfalls of bumping a version number.</p>


<p>Debian user Pétùr asked for help with a <a href="https://lists.debian.org/debian-user/2018/09/msg00311.html">File with weird permissions, impossible to delete</a>.
The discussion moves quick onto permission issues, inodes, fsck, and bad SATA 
cabling.</p>


<p>Debian user Subhadip Ghosh asked, <a href="https://lists.debian.org/debian-user/2018/09/msg00700.html">Why does Debian allow all incoming traffic by default?</a></p>


<p><b>팁과 트릭</b></p>

<p>Jonathan McDowell continues his series of write ups on home automation with
<a href="https://www.earth.li/~noodles/blog/2018/10/heating-automation.html">Controlling my heating with Home Assistant</a> and <a href="https://www.earth.li/~noodles/blog/2018/09/netlink-arp-presence.html">Using ARP via netlink to detect
presence.</a></p>

<p>Antoine Beaupré shared tips for <a href="https://anarc.at/blog/2018-10-04-archiving-web-sites/">Archiving web sites</a> using freely available
tools and some knowledge.</p>

<p>Sergio Alberti <a href="https://sergioalberti.gitlab.io//gsoc/debian/2018/09/24/reveng.html">shared</a> a guide on <a href="https://reverse-engineering-ble-devices.readthedocs.io/en/latest/">Reverse Engineering Bluetooth Low Energy Devices</a></p>

<p>Petter Reinholdtsen <a href="http://people.skolelinux.org/pere/blog/VLC_in_Debian_now_can_do_bittorrent_streaming.html">shares VLC in Debian now can do bittorrent streaming</a>.</p>

<p>Laura Arjona Reina found a small digital photo frame and brings it back to
use in <a href="https://larjona.wordpress.com/2018/09/22/handling-an-old-digital-photo-frame-ax203-with-debian-and-gphoto2/">Handling an old Digital Photo Frame (AX203) with Debian (and gphoto2)</a>.</p>


<p><b>Once upon a time in Debian:</b></p>

## Items pulled from the Timeline https://timeline.debian.net
## Jump to any random year/ same month/ same week.
## Provide link and link description.
## This may work better with a script at some point, but for now let us see
## what the ease of work is.

## Format - YYYY-MM-DD text

<ul>
<li>2009-12-08 <a href="https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=560000">Debian Bug &#35;560000 reported by Mika Tiainen</a></li>
<li>2016-12-10 <a href="https://miniconf.debian.or.jp/index.en.html">MiniDebconf 2016 held in Tokyo, Japan</a></li>
<li>1996-12-12 <a href="https://lists.debian.org/debian-devel-announce/2014/08/msg00012.html">Debian 1.2 Released (Rex)</a></li>
<li>2014-12-13 <a href="https://wiki.debian.org/BSP/2014/12/nl/Tilburg">Bug Squashing Party in Tilburg, The Netherlands</a></li>
<li>2016-12-13 <a href="https://reproducible-builds.org/events/berlin2016/">Debian co-organizes and sponsors Reproducible Builds Summit in Berlin, Germany</a></li>
</ul>


<toc-add-entry name="outside">Outside News</toc-add-entry>

<p>
The Creative Commons Global Summit will be held in Lisbon, Portugal May 9-11 2019. Their Call for Proposals is open until December 10th 2018.
Visit <a href="https://summit.creativecommons.org/">https://summit.creativecommons.org/</a> for more details and registration.
</p>

<toc-add-entry name="quicklinks">Quick Links from Debian Social Media</toc-add-entry>

<p>
This is an extract from the
<a href="https://micronews.debian.org">micronews.debian.org</a> feed, in
which we have removed the topics already commented on in this DPN issue.
You can skip this section if you already follow <b>micronews.debian.org</b>
or the <b>@debian</b> profile in a social network (Pump.io, GNU Social, 
Mastodon or Twitter). The items are provided unformatted in descending order by date
(recent news at the top).
</p>

<p>
<b>11월</b></p>
<ul>
<li>Bits from the @Debian Project Leader (November 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/11/msg00007.html">https://lists.debian.org/debian-devel-announce/2018/11/msg00007.html</a>
</li>

<li>Debian CI pipeline for Debian Maintainers!
<a href="https://salsa.debian.org/salsa-ci-team/pipeline/blob/master/README.md">https://salsa.debian.org/salsa-ci-team/pipeline/blob/master/README.md</a>
</li>

<li>Happy birthday @Fedora!
<a href="https://fedoramagazine.org/celebrate-fifteen-years-fedora/">https://fedoramagazine.org/celebrate-fifteen-years-fedora/</a>
</li>

<li>Perl 5.28 transition underway, wide uninstallability is to be expected in sid for the next days!
<a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00006.html">https://lists.debian.org/debian-devel-announce/2018/10/msg00006.html</a>
</li>
</ul>


<p>
<b>10월</b></p>
<ul>
<li>Bits from the Debian Project Leader (October 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/10/msg00005.html">https://lists.debian.org/debian-devel-announce/2018/10/msg00005.html</a>
</li>

<li>"Salsa ribbons" by Chris Lamb
<a href="https://chris-lamb.co.uk/posts/salsa-ribbons">https://chris-lamb.co.uk/posts/salsa-ribbons</a>
</li>

<li>[debian-installer] Call to update translations for Buster
<a href="https://lists.debian.org/debian-i18n/2018/10/msg00002.html">https://lists.debian.org/debian-i18n/2018/10/msg00002.html</a>
</li>

<li>Bits from MicroDebConf Brasília 2018
<a href="http://blog.kanashiro.xyz/debconf/2018/09/28/microdebconf-bsb.html">http://blog.kanashiro.xyz/debconf/2018/09/28/microdebconf-bsb.html</a>
</li>
</ul>


<p>
<b>9월</b></p>
<ul>
<li>Bits from the Debian Project Leader (September 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/09/msg00005.html">https://lists.debian.org/debian-devel-announce/2018/09/msg00005.html</a>
</li>

<li>Call for mentors and project ideas for next Outreachy round
<a href="https://lists.debian.org/debian-outreach/2018/09/msg00030.html">https://lists.debian.org/debian-outreach/2018/09/msg00030.html</a>
</li>

<li>Debian security repositories stay online in Japan despite magnitude 6.7 earthquake
<a href="https://henrich-on-debian.blogspot.com/2018/09/earthquake-struck-hokkaido-and-caused.html">https://henrich-on-debian.blogspot.com/2018/09/earthquake-struck-hokkaido-and-caused.html</a>
</li>
</ul>


<p>
<b>8월</b></p>
<ul>
<li>FISL19 in Porto Alegre will take place before DebCamp next year
<a href="https://debconf19.debconf.org/news/2018-08-23-fisl19/">https://debconf19.debconf.org/news/2018-08-23-fisl19/</a>
</li>

<li>Bits from the Debian Project Leader (August 2018)
<a href="https://lists.debian.org/debian-devel-announce/2018/08/msg00008.html">https://lists.debian.org/debian-devel-announce/2018/08/msg00008.html</a>
</li>
</ul>


<toc-add-entry name="continuedpn">Want to continue reading DPN?</toc-add-entry>
<continue-dpn />

<p><a href="https://lists.debian.org/debian-news/">Subscribe or Unsubscribe</a> from the Debian News mailing list</p>

#use wml::debian::projectnews::footer editor="The Publicity Team with contributions from Jean-Pierre Giraud, Justin B Rye, Laura Arjona Reina"
# Please add the contributors to the /dpn/CREDITS file
# Translators may also add a translator="foo, bar, baz" to the previous line
