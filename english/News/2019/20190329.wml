<define-tag pagetitle>Handshake donates $300,000 USD to Debian</define-tag>
<define-tag release_date>2019-03-29</define-tag>
#use wml::debian::news
# $Id$

<p>
In 2018 the Debian project received a donation of $300,000 USD from
<a href="https://handshake.org/">Handshake</a>, an organization developing an
experimental peer-to-peer root domain naming system.
</p>

<p>
This significant financial contribution will help Debian to continue the
hardware replacement plan designed by the
<a href="https://wiki.debian.org/Teams/DSA">Debian System Administrators</a>,
renewing servers and other hardware components and thus making the
development and community infrastructure of the Project more reliable.
</p>

<p>
<q><em>A deep and sincere thank you to the Handshake Foundation for their
support of the Debian Project.</em></q> said Chris Lamb, Debian Project Leader.
<q><em>Contributions such as theirs make it possible for a large number of
diverse contributors from all over the world to work together towards our
mutual goal of developing a fully-free <q>universal</q> operating system.</em></q>
</p>

<p>
Handshake is a decentralized, permissionless naming protocol compatible
with DNS where every peer is validating and in charge of managing the
root zone with the goal of creating an alternative to existing
Certificate Authorities.
</p>

<p>
The Handshake project, its sponsors and contributors recognize Free and
Open Source Software as a crucial part of the foundations of the
Internet and their project, so they decided to reinvest back into free
software by gifting $10,200,000 USD to various FLOSS developers and projects,
as well as non-profit organizations and universities that support free software development.
</p>

<p>
Thank you very much, Handshake, for your support!
</p>

<h2>About Debian</h2>
<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>
<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.</p>
