<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Tobias Stoeckmann discovered that su does not properly handle clearing a
child PID. A local attacker can take advantage of this flaw to send
SIGKILL to other processes with root privileges, resulting in denial of
service.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
4.1.5.1-1+deb7u1.</p>

<p>We recommend that you upgrade your shadow packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-838.data"
# $Id: $
