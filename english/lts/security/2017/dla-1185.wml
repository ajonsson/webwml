<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that sam2p, a utility to convert raster images and
other image formats, was affected by an integer overflow vulnerability
with resultant heap-based buffer overflow in input-bmp.ci because
width and height multiplications occur unsafely. This may lead to an
application crash or unspecified other impact when a maliciously
crafted file is processed.</p>

<p>For Debian 7 <q>Wheezy</q>, these problems have been fixed in version
0.49.1-1+deb7u2.</p>

<p>We recommend that you upgrade your sam2p packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2017/dla-1185.data"
# $Id: $
