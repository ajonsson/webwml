<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>It was discovered that there were multiple vulnerabilities in the
<q>zsh</q> shell:</p>

<ul>
  <li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10070">CVE-2014-10070</a>
    <p>Fix a privilege-elevation issue if the
    environment has not been properly sanitized.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10071">CVE-2014-10071</a>
    <p>Prevent a buffer overflow for very long file
    descriptors in the <q>&gt;&amp; fd</q> syntax.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2014-10072">CVE-2014-10072</a>
    <p>Correct a buffer overflow when scanning very long
    directory paths for symbolic links.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2016-10714">CVE-2016-10714</a>
    <p>Fix an off-by-one error that was resulting in
    undersized buffers that were intended to support PATH_MAX.</p></li>

  <li><a href="https://security-tracker.debian.org/tracker/CVE-2017-18206">CVE-2017-18206</a>
    <p>Fix a buffer overflow in symlink expansion.</p></li>

</ul>

<p>For Debian 7 <q>Wheezy</q>, this issue has been fixed in zsh version
4.3.17-1+deb7u1.</p>

<p>We recommend that you upgrade your zsh packages.</p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2018/dla-1304.data"
# $Id: $
