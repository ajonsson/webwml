<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>
<p>Claudio Bozzato of Cisco Talos discovered an exploitable integer overflow
vulnerability in the flattenIncrementally function in the xcf2png and xcf2pnm
binaries of xcftools. An integer overflow can occur while walking through tiles
that could be exploited to corrupt memory and execute arbitrary code. In order
to trigger this vulnerability, a victim would need to open a specially crafted
XCF file.</p>

<p>For Debian 9 stretch, these problems have been fixed in version
1.0.7-6+deb9u1.</p>

<p>We recommend that you upgrade your xcftools packages.</p>

<p>For the detailed security status of xcftools please refer to
its security tracker page at:
<a rel="nofollow" href="https://security-tracker.debian.org/tracker/xcftools">https://security-tracker.debian.org/tracker/xcftools</a></p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a rel="nofollow" href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2021/dla-2553.data"
# $Id: $
