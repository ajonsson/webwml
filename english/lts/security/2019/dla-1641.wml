<define-tag description>LTS security update</define-tag>
<define-tag moreinfo>

<p>Several stack exhaustion conditions were found in mxml that can easily
crash when parsing xml files.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4570">CVE-2016-4570</a>

    <p>The mxmlDelete function in mxml-node.c allows remote attackers to
    cause a denial of service (stack consumption) via crafted xml file.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2016-4571">CVE-2016-4571</a>

    <p>The mxml_write_node function in mxml-file.c allows remote attackers
    to cause a denial of service (stack consumption) via crafted xml
    file</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-20004">CVE-2018-20004</a>

    <p>A stack-based buffer overflow in mxml_write_node via vectors
    involving a double-precision floating point number.</p></li>

</ul>

<p>For Debian 8 <q>Jessie</q>, these problems have been fixed in version
2.6-2+deb8u1.</p>

<p>We recommend that you upgrade your mxml packages.</p>

<p>Further information about Debian LTS security advisories, how to apply
these updates to your system and frequently asked questions can be
found at: <a href="https://wiki.debian.org/LTS">https://wiki.debian.org/LTS</a></p>

</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/lts/security/2019/dla-1641.data"
# $Id: $
