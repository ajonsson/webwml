#use wml::debian::translation-check translation="0f4f3e8210b7403850a5550da354ea08036f8c2f"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium:</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21148">CVE-2021-21148</a>

    <p>Mattias Buelens descubrió un problema de desbordamiento de memoria en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21149">CVE-2021-21149</a>

    <p>Ryoya Tsukasaki descubrió un problema de desbordamiento de pila en la implementación de
    Data Transfer.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21150">CVE-2021-21150</a>

    <p>Woojin Oh descubrió un problema de «uso tras liberar» en la descarga de ficheros.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21151">CVE-2021-21151</a>

    <p>Khalil Zhani descubrió un problema de «uso tras liberar» en el sistema de pagos.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21152">CVE-2021-21152</a>

    <p>Se descubrió un desbordamiento de memoria en la gestión de medios.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21153">CVE-2021-21153</a>

    <p>Jan Ruge descubrió un problema de desbordamiento de pila en el proceso de la GPU.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21154">CVE-2021-21154</a>

    <p>Abdulrahman Alqabandi descubrió un problema de desbordamiento de memoria en la implementación
    de Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21155">CVE-2021-21155</a>

    <p>Khalil Zhani descubrió un problema de desbordamiento de memoria en la implementación
    de Tab Strip.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21156">CVE-2021-21156</a>

    <p>Sergei Glazunov descubrió un problema de desbordamiento de memoria en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2021-21157">CVE-2021-21157</a>

    <p>Se descubrió un problema de «uso tras liberar» en la implementación de Web Sockets.</p></li>

</ul>

<p>Para la distribución «estable» (buster), estos problemas se han corregido en
la versión 88.0.4324.182-1~deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium.</p>

<p>Para información detallada sobre el estado de seguridad de chromium, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium">\
https://security-tracker.debian.org/tracker/chromium</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2021/dsa-4858.data"
