#use wml::debian::translation-check translation="c1ea0c532237ebe87381e982acdfd0b88c70ad4f"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Nick Roessler y Rafi Rubin descubrieron que los analizadores sintácticos para los protocolos
IMAP y ManageSieve del servidor de correo electrónico Dovecot no validan correctamente
la entrada (tanto antes como después de la identificación o «login»). Un atacante remoto puede aprovechar
este defecto para desencadenar escrituras en la memoria dinámica («heap») fuera de límites, dando lugar a
fugas de información o, potencialmente, a ejecución de código arbitrario.</p>

<p>Para la distribución «antigua estable» (stretch), este problema se ha corregido
en la versión 1:2.2.27-3+deb9u5.</p>

<p>Para la distribución «estable» (buster), este problema se ha corregido en
la versión 1:2.3.4.1-5+deb10u1.</p>

<p>Le recomendamos que actualice los paquetes de dovecot.</p>

<p>Para información detallada sobre el estado de seguridad de dovecot, consulte su página
en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/dovecot">\
https://security-tracker.debian.org/tracker/dovecot</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2019/dsa-4510.data"
