#use wml::debian::translation-check translation="2fffb6d907665c5179287a533ba1d4ef4412e32a"
<define-tag description>actualización de seguridad</define-tag>
<define-tag moreinfo>
<p>Se han descubierto varias vulnerabilidades en el navegador web Chromium.</p>

<ul>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17480">CVE-2018-17480</a>

    <p>Guang Gong descubrió un problema de escritura fuera de límites en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-17481">CVE-2018-17481</a>

    <p>Se descubrieron varios problemas de «uso tras liberar» en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18335">CVE-2018-18335</a>

    <p>Se descubrió un problema de desbordamiento de memoria en la biblioteca skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18336">CVE-2018-18336</a>

    <p>Huyna descubrió un problema de «uso tras liberar» en la biblioteca pdfium.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18337">CVE-2018-18337</a>

    <p>cloudfuzzer descubrió un problema de «uso tras liberar» en blink/webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18338">CVE-2018-18338</a>

    <p>Zhe Jin descubrió un problema de desbordamiento de memoria en el visualizador de canvas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18339">CVE-2018-18339</a>

    <p>cloudfuzzer descubrió un problema de «uso tras liberar» en la implementación de
    WebAudio.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18340">CVE-2018-18340</a>

    <p>Se descubrió un problema de «uso tras liberar» en la implementación de MediaRecorder.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18341">CVE-2018-18341</a>

    <p>cloudfuzzer descubrió un problema de desbordamiento de memoria en blink/webkit.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18342">CVE-2018-18342</a>

    <p>Guang Gong descubrió un problema de escritura fuera de límites en la biblioteca
    javascript v8.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18343">CVE-2018-18343</a>

    <p>Tran Tien Hung descubrió un problema de «uso tras liberar» en la biblioteca skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18344">CVE-2018-18344</a>

    <p>Jann Horn descubrió un error en la implementación de Extensions.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18345">CVE-2018-18345</a>

    <p>Masato Kinugawa y Jun Kokatsu descubrieron un error en la funcionalidad Site
    Isolation («aislamiento de sitios web»).</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18346">CVE-2018-18346</a>

    <p>Luan Herrera descubrió un error en la interfaz de usuario.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18347">CVE-2018-18347</a>

    <p>Luan Herrera descubrió un error en la implementación de Navigation.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18348">CVE-2018-18348</a>

    <p>Ahmed Elsobky descubrió un error en la implementación de omnibox.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18349">CVE-2018-18349</a>

    <p>David Erceg descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18350">CVE-2018-18350</a>

    <p>Jun Kokatsu descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18351">CVE-2018-18351</a>

    <p>Jun Kokatsu descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18352">CVE-2018-18352</a>

    <p>Jun Kokatsu descubrió un error en la gestión de Media.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18353">CVE-2018-18353</a>

    <p>Wenxu Wu descubrió un error en la implementación de la autenticación de red.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18354">CVE-2018-18354</a>

    <p>Wenxu Wu descubrió un error relacionado con la integración con GNOME Shell.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18355">CVE-2018-18355</a>

    <p>evil1m0 descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18356">CVE-2018-18356</a>

    <p>Tran Tien Hung descubrió un problema de «uso tras liberar» en la biblioteca skia.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18357">CVE-2018-18357</a>

    <p>evil1m0 descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18358">CVE-2018-18358</a>

    <p>Jann Horn descubrió un error de imposición de reglas.</p></li>

<li><a href="https://security-tracker.debian.org/tracker/CVE-2018-18359">CVE-2018-18359</a>

    <p>cyrilliu descubrió un problema de lectura fuera de límites en la biblioteca
    javascript v8.</p></li>

</ul>

<p>Esta actualización corrige también algunos otros problemas relevantes de seguridad
que todavía no han recibido identificadores CVE.</p>

<p>Para la distribución «estable» (stretch), estos problemas se han corregido en
la versión 71.0.3578.80-1~deb9u1.</p>

<p>Le recomendamos que actualice los paquetes de chromium-browser.</p>

<p>Para información detallada sobre el estado de seguridad de chromium-browser, consulte
su página en el sistema de seguimiento de problemas de seguridad:
<a href="https://security-tracker.debian.org/tracker/chromium-browser">\
https://security-tracker.debian.org/tracker/chromium-browser</a></p>
</define-tag>

# do not modify the following line
#include "$(ENGLISHDIR)/security/2018/dsa-4352.data"
