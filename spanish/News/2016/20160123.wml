#use wml::debian::translation-check translation="ea6a13e28f99e15f67ccfd6b74b4cc4bb185fdbd"
<define-tag pagetitle>Debian 8 actualizado: publicada la versión 8.3</define-tag>
<define-tag release_date>2016-01-23</define-tag>
#use wml::debian::news

<define-tag release>8</define-tag>
<define-tag codename>jessie</define-tag>
<define-tag revision>8.3</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>El proyecto Debian se complace en anunciar la tercera actualización de su
distribución «estable» Debian <release> (nombre en clave <q><codename></q>).
Esta versión añade, principalmente, correcciones de problemas de seguridad a la distribución
«estable», junto con unos pocos ajustes para problemas graves. Los avisos de seguridad
se han publicado ya de forma independiente, y aquí hacemos referencia a ellos donde corresponde.</p>

<p>Tenga en cuenta que esta actualización no constituye una nueva versión completa de Debian
<release>, solo actualiza algunos de los paquetes incluidos. No es
necesario deshacerse de los viejos CD o DVD de <q><codename></q>, basta con actualizar un
sistema Debian ya instalado, utilizando una réplica que esté al día, para que los
paquetes instalados de los que haya una versión posterior se actualicen.</p>

<p>Quienes instalen frecuentemente actualizaciones desde security.debian.org no tendrán
que actualizar muchos paquetes, y la mayoría de dichas actualizaciones están
incluidas en esta nueva versión.</p>

<p>Pronto habrá disponibles nuevos medios de instalación e imágenes de CD y de DVD con
paquetes actualizados en los sitios habituales.</p>

<p>La actualización en línea a esta versión se realiza habitualmente haciendo que
la herramienta de gestión de paquetes aptitude (o apt) apunte a una de las muchas réplicas
FTP o HTTP de Debian (vea la página del manual sources.list(5)). En la dirección siguiente
puede encontrar el listado completo de réplicas:</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrección de fallos varios</h2>

<p>Esta actualización de la distribución «estable» añade correcciones importantes a los paquetes
siguientes:</p>
<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>
<correction android-platform-frameworks-base "[i386] recompilado para corregir dependencia con android-libhost">
<correction apache2                          "Corrige split-logfile para que funcione con el perl actual, secondary-init-script para no incluir el script de inicio principal con 'set -e', las pruebas de conmutación a MPM aplazada; añade Reemplaza/Rompe con número de versión para libapache2-mod-macro">
<correction apt                              "Oculta el primer mensaje de nivel de depuración relativo a fallo de fusión de pdiff; corrige el marcado de dependencias de paquetes en APT::Never-MarkAuto-Sections como manual; no analiza sintácticamente campos Status de orígenes remotos">
<correction apt-dater-host                   "Corrige detección de la versión del núcleo">
<correction apt-offline                      "Añade dependencia con python-apt, que faltaba">
<correction arb                              "Omite la comprobación de la versión del compilador">
<correction augeas                           "HTTPD lense: incluye directorio /etc/apache2/conf-available, permite comentarios EOL tras etiquetas de sección">
<correction base-files                       "Actualizado para la versión 8.3; os-release: elimina la barra del final en la variable SUPPORT_URL">
<correction bcfg2                            "Soporta Django 1.7">
<correction ben                              "Corrige enlaces compactos a buildd.debian.org; ignora potenciales errores al borrar fichero de serialización («lock file»); llama a dose-debcheck con --deb-native-arch">
<correction ca-certificates                  "Actualiza el lote de autoridades certificadoras de Mozilla a la versión 2.6">
<correction ceph                             "Codifica en la URL en nombre del «bucket» [CVE-2015-5245]">
<correction charybdis                        "Corrección de seguridad [CVE-2015-5290]; inicializa gnutls correctamente">
<correction chrony                           "Depende de libcap-dev en tiempo de compilación para permitir la eliminación de privilegios">
<correction commons-httpclient               "Garantiza que las llamadas HTTPS utilicen http.socket.timeout durante el «handshake» SSL [CVE-2015-5262]">
<correction cpuset                           "Actualiza parche de prefijo del espacio de nombres del sistema de archivos">
<correction curlftpfs                        "Impide conversión insegura de tipo para getpass() en arquitecturas de 64 bits">
<correction dbconfig-common                  "Corrige permisos de ficheros de respaldo de PostgreSQL">
<correction debian-handbook                  "Actualizado para Jessie">
<correction debian-installer                 "Incluye de nuevo imágenes del instalador para QNAP TS-x09; proporciona imágenes u-boot para computadoras de enchufe; añade el módulo part_gpt en la imagen grub central; añade beep al menú de arranque de x86 UEFI; añade la 's' como tecla abreviada para «speech» al menú de arranque de x86 UEFI; excluye explícitamente usb-serial-modules de la imagen de network-console armel y usb-modules de network-console en armel/orion5x; elimina la extensión de fichero del initrd para dispositivos QNAP; ajusta el soporte p-u para gestionar solo file:// en lugar de (f|ht)tp://">
<correction debian-installer-netboot-images  "Recompilado para esta versión">
<correction docbook2x                        "No instala ficheros info/dir.gz">
<correction doctrine                         "Corrige problema de permisos de directorios [CVE-2015-5723]">
<correction drbd-utils                       "Corrige configuración de drbdadm con direcciones de pares IPv6">
<correction ejabberd                         "Corrige consultas LDAP rotas">
<correction exfat-utils                      "Corrige desbordamiento de memoria y bucle infinito">
<correction exim4                            "Corrige algunas caídas relacionadas con ACL MIME; corrige un fallo que provoca envíos duplicados, especialmente en conexiones TLS">
<correction fglrx-driver                     "Nueva versión del proyecto original; corrige problema de seguridad [CVE-2015-7724]">
<correction file                             "Corrige gestión de --parameter">
<correction flash-kernel                     "Evita que espere a Ctrl-C si está en uso alguna interfaz de debconf">
<correction fuse-exfat                       "Corrige desbordamiento de memoria y bucle infinito">
<correction ganglia-modules-linux            "Reinicia el servicio ganglia tras la instalación solo si estaba en ejecución previamente">
<correction getmail4                         "Establece poplib._MAXLINE=1MB">
<correction glance                           "Impide que se modifique directamente el estado de la imagen por medio de la API v1 [CVE-2015-5251]">
<correction glibc                            "Corrige el fallo por el que, con nscd, getaddrinfo en ocasiones devuelve datos no inicializados; corrige corrupción de datos al leer la base de datos files de NSS [CVE-2015-5277]; corrige desbordamiento de memoria (lectura más allá del final de la zona de memoria) en internal_fnmatch; corrige desbordamiento de entero _IO_wstr_overflow; corrige cierre inesperado de bases de datos nss_files tras búsquedas, provocando denegación de servicio [CVE-2014-8121]; corrige caché de netgroups NSCD; inhabilita de forma incondicional LD_POINTER_GUARD; modifica («mangle») los punteros a funciones en tls_dtor_list; corrige problemas de asignación de memoria que pueden dar lugar a desbordamientos de pila; actualiza la lista negra TSX para incluir también algunas CPU Broadwell">
<correction gnome-orca                       "Asegura el foco correcto al introducir la contraseña, de forma que no se muestren los caracteres introducidos">
<correction gnome-shell-extension-weather    "Muestra un aviso si el usuario no ha proporcionado la clave de la API, puesto que la consulta a openweathermap.org ya no funciona sin dicha clave">
<correction gummi                            "Evita el uso de nombres predecibles para nombrar ficheros temporales [CVE 2015-7758]">
<correction human-icon-theme                 "debian/clean-up.sh: no ejecuta procesos en segundo plano">
<correction ieee-data                        "Actualiza los ficheros de datos incluidos, añadiendo mam.txt y oui36.txt; deja de descargar vía HTTPS, puesto que ni wget ni curl soportan TLS AIA, usado en la actualidad por standards.ieee.org">
<correction intel-microcode                  "Actualiza el microcódigo incluido">
<correction iptables-persistent              "Deja de utilzar ficheros de reglas con permiso de lectura universal; reescrito README">
<correction isc-dhcp                         "Corrige error cuando se utiliza el máximo tiempo límite de concesión («lease time») en sistemas de 64 bits">
<correction keepassx                         "Corrige almacenamiento de contraseñas como texto en claro [CVE-2015-8378]">
<correction libapache-mod-fastcgi            "Cambia B-D de libtool a libtool-bin para corregir error de compilación">
<correction libapache2-mod-perl2             "Corrige caídas en modperl_interp_unselect()">
<correction libcgi-session-perl              "Marca como seguros («untaint») datos brutos provenientes de backends de almacenamiento de sesión, corrigiendo una regresión provocada por las correcciones para CVE-2015-8607 en perl">
<correction libdatetime-timezone-perl        "Nueva versión del proyecto original">
<correction libencode-perl                   "Gestiona correctamente una ausencia de marca de orden de bytes (BOM, por sus siglas en inglés) al decodificar">
<correction libhtml-scrubber-perl            "Corrige vulnerabilidad de ejecución de scripts entre sitios («cross-site scripting») en comentarios [CVE-2015-5667]">
<correction libinfinity                      "Corrige posibles caídas cuando se elimina una entrada del navegador de documentos y cuando están activas las listas de control de acceso">
<correction libiptables-parse-perl           "Corrige el uso de nombres predecibles para ficheros temporales [CVE-2015-8326]">
<correction libraw                           "Corrige desbordamiento de índice en smal_decode_segment [CVE-2015-8366]; corrige objetos de memoria no inicializados correctamente [CVE-2015-8367]">
<correction libssh                           "Corrige <q>desreferencia de puntero nulo debida a un error lógico en la gestión de paquetes SSH_MSG_NEWKEYS y KEXDH_REPLY</q> [CVE-2015-3146]">
<correction linux                            "Actualizado a la versión 3.16.7-ctk20 del proyecto original; nbd: restaura detección de exceso de tiempo («timeout») en solicitudes; [x86] habilita PINCTRL_BAYTRAIL; [mips*/octeon] habilita CAVIUM_CN63XXP1; firmware_class: corrige condición en bucle de búsqueda de directorios; [x86] KVM: svm: intercepta #DB incondicionalmente [CVE-2015-8104]">
<correction linux-tools                      "Añade nuevo paquete hyperv-daemons">
<correction lldpd                            "Corrige una violación de acceso y un error de aserción al recibir direcciones de gestión LLDP mal formadas">
<correction madfuload                        "Usa autoreconf -fi para corregir error de compilación con automake 1.14">
<correction mdadm                            "Inhabilita el ensamblado incremental, ya que puede provocar problemas al arrancar un RAID degradado">
<correction mkvmlinuz                        "Dirige la salida de run-parts a la salida de error estándar («stderr»)">
<correction monit                            "Corrige regresión de la 5.8.1 relacionada con máscaras de usuario («umask»)">
<correction mpm-itk                          "Corrige un problema cuando se intentaba el cierre de conexiones en el padre. Esto daba lugar a que no se respetara <q>Connection: close</q> y a varios efectos extraños con SSL keepalive en determinados navegadores">
<correction multipath-tools                  "Corrige el descubrimiento de dispositivos que tienen el atributo de sysfs en blanco; añade documentación para abarcar escenarios con nombres significativos («friendly names») adicionales; init: corrige problema que impide la parada cuando no se encuentra dispositivo raíz; usa 'SCSI_IDENT_.*' como lista blanca («whitelist») de propiedades por omisión">
<correction netcfg                           "Corrige is_layer3_qeth en s390x para evitar que abandone si el controlador de red no es qeth">
<correction nvidia-graphics-drivers          "Nueva versión del proyecto original [CVE-2015-5950]; corrige problema de entrada no saneada en modo usuario [CVE-2015-7869]">
<correction nvidia-graphics-drivers-legacy-304xx "Nueva versión del proyecto original; corrige problema de entrada no saneada en modo usuario [CVE-2015-7869]">
<correction nvidia-graphics-modules          "Recompilado con nvidia-kernel-source 340.96">
<correction openldap                         "Corrige caída al añadir un valor grande de atributo con el componente de registro para auditoría («auditlog overlay») habilitado">
<correction openvpn                          "Añade --no-block a script if-up.d para evitar que se bloquee el arranque en interfaces con instancias openvpn">
<correction owncloud                         "Corrige inclusión de fichero local en Microsoft Windows Platform [CVE-2015-4716], agotamiento de recursos al sanear nombres de ficheros [CVE-2015-4717], inyección de órdenes cuando se usa almacenamiento SMB externo [CVE-2015-4718], calendar export: elusión de autorización mediante clave controlada por el usuario [CVE-2015-6670]; corrige XSS reflejado en descubrimiento de proveedores OCS [oc-sa-2016-001] [CVE-2016-1498], revelación de ficheros cuyo nombre empieza por \<q>.v\</q> por no comprobar un código de retorno [oc-sa-2016-003] [CVE-2016-1500], exposición de información mediante los listados de directorios en el explorador de ficheros [oc-sa-2016-002] [CVE-2016-1499] y revelación de la ruta de instalación a través de mensajes de error [oc-sa-2016-004] [CVE-2016-1501]">
<correction pam                              "Corrige denegación de servicio y enumeración de usuarios debido a un bloqueo de «tubería» («pipe») en pam_unix [CVE-2015-3238]">
<correction pcre3                            "Corrige problemas de seguridad [CVE-2015-2325 CVE-2015-2326 CVE-2015-3210 CVE-2015-5073 CVE-2015-8384 CVE-2015-8388]">
<correction pdns                             "Corrige actualizaciones con la configuración por omisión">
<correction perl                             "Gestiona correctamente una ausencia de marca de orden de bytes (BOM, por sus siglas en inglés) al decodificar">
<correction php-auth-sasl                    "Recompilado con pkg-php-tools 1.28 para corregir dependencias de PHP">
<correction php-doctrine-annotations         "Corrige problema de permisos de directorios [CVE-2015-5723]">
<correction php-doctrine-cache               "Corrige problema de permisos de ficheros y directorios [CVE-2015-5723]">
<correction php-doctrine-common              "Corrige problema de permisos de ficheros [CVE-2015-5723]">
<correction php-dropbox                      "Rehúsa tratar ficheros que contengan una @ [CVE-2015-4715]">
<correction php-mail-mimedecode              "Recompilado con pkg-php-tools 1.28 para corregir dependencias de PHP">
<correction php5                             "Nueva versión del proyecto original">
<correction plowshare4                       "Inhabilita soporte de Javascript">
<correction postgresql-9.1                   "Nueva versión del proyecto original">
<correction pykerberos                       "Añade soporte de verificación de autenticidad del KDC [CVE-2015-3206]">
<correction python-yaql                      "Elimina el paquete python3-yaql, que está roto">
<correction qpsmtpd                          "Corrige problema de compatibilidad con versiones de Net::DNS más recientes">
<correction quassel                          "Corrige denegación de servicio remota en quassel core desencadenada mediante la orden /op * [CVE-2015-8547]">
<correction redis                            "Garantiza que se cree un directorio de ejecución («runtime directory») válido en ejecuciones bajo systemd">
<correction redmine                          "Corrige las actualizaciones en presencia de extensiones («plugins») instaladas localmente; corrige movimiento de peticiones («issues») entre proyectos">
<correction rsyslog                          "Corrige caída en el módulo imfile cuando se utiliza el modo inotify; evita violación de acceso en la creación de un dynafile">
<correction ruby-bson                        "Corrige denegación de servicio y posible inyección [CVE-2015-4410]">
<correction s390-dasd                        "Si no encuentra ningún canal, termina sin errores. Esto permite que s390-dasd se haga a un lado en VMs con discos virtio">
<correction shadow                           "Corrige gestión de errores en la detección de usuarios ocupados">
<correction sparse                           "Corrige error de compilación con llvm-3.5">
<correction spip                             "Corrige problema de ejecución de scripts entre sitios («cross-site scripting»)">
<correction stk                              "Instala los ficheros SKINI.{msg,tbl}, que faltaban">
<correction sus                              "Actualiza las sumas de verificación («checksums») para el fichero comprimido («tarball») del proyecto original">
<correction swift                            "Corrige borrado no autorizado de versiones de objetos Swift [CVE-2015-1856]; corrige fuga de información por medio de tempurls Swift [CVE-2015-5223]; corrige nombre de servicio del servicio de expiración de objetos en script de inicio; añade el script de inicio container-sync; estandariza (<q>standardise</q>) la adición de usuarios">
<correction systemd                          "Corrige rotura de espacio de nombres por ordenamiento incorrecto de rutas; no falla por exceso de tiempo («timeout») transcurridos 90 segundos cuando no se ha introducido contraseña para dispositivos cryptsetup; establece la zona horaria del núcleo solo cuando el RTC se ejecuta con la hora local, evitando posibles retrocesos de la hora; corrige gestión incorrecta del separador coma en systemd-delta; hace que el comportamiento de la difusión («broadcast») DHCP sea configurable en systemd-networkd">
<correction tangerine-icon-theme             "debian/clean-up.sh: no ejecuta procesos en segundo plano">
<correction torbrowser-launcher              "Aplica realmente los parches de 0.1.9-1+deb8u1; deja de confinar el script start-tor-browser con AppArmor; establece los perfiles AppArmor usr.bin.torbrowser-launcher en modo «queja» («complain mode»)">
<correction ttylog                           "Corrige truncamiento de nombre de dispositivo al seleccionar dispositivo">
<correction tzdata                           "Nueva versión del proyecto original">
<correction uqm                              "Añade indicador -lm, que faltaba, lo que corrige error de compilación">
<correction vlc                              "Nueva versión «estable» del proyecto original">
<correction webkitgtk                        "Nueva versión «estable» del proyecto original; corrige <q>verificación tardía de certificado TLS</q> [CVE-2015-2330]">
<correction wxmaxima                         "Evita caída al encontrar paréntesis en diálogos">
<correction zendframework                    "Corrige problema de entropía con captcha [ZF2015-09]">
</table>

<h2>Actualizaciones de seguridad</h2>


<p>Esta versión añade las siguientes actualizaciones de seguridad a la distribución
«estable». El equipo de seguridad ya ha publicado un aviso para cada una
de estas actualizaciones:</p>

<table border=0>
<tr><th>ID del aviso</th>  <th>Paquete</th></tr>


<dsa 2015 3208 freexl>
<dsa 2015 3235 openjdk-7>
<dsa 2015 3280 php5>
<dsa 2015 3311 mariadb-10.0>
<dsa 2015 3316 openjdk-7>
<dsa 2015 3324 icedove>
<dsa 2015 3327 squid3>
<dsa 2015 3332 wordpress>
<dsa 2015 3337 gdk-pixbuf>
<dsa 2015 3344 php5>
<dsa 2015 3346 drupal7>
<dsa 2015 3347 pdns>
<dsa 2015 3348 qemu>
<dsa 2015 3350 bind9>
<dsa 2015 3351 chromium-browser>
<dsa 2015 3352 screen>
<dsa 2015 3353 openslp-dfsg>
<dsa 2015 3354 spice>
<dsa 2015 3355 libvdpau>
<dsa 2015 3356 openldap>
<dsa 2015 3357 vzctl>
<dsa 2015 3358 php5>
<dsa 2015 3359 virtualbox>
<dsa 2015 3360 icu>
<dsa 2015 3361 qemu>
<dsa 2015 3363 owncloud-client>
<dsa 2015 3364 linux>
<dsa 2015 3365 iceweasel>
<dsa 2015 3366 rpcbind>
<dsa 2015 3367 wireshark>
<dsa 2015 3368 cyrus-sasl2>
<dsa 2015 3369 zendframework>
<dsa 2015 3370 freetype>
<dsa 2015 3371 spice>
<dsa 2015 3373 owncloud>
<dsa 2015 3374 postgresql-9.4>
<dsa 2015 3375 wordpress>
<dsa 2015 3376 chromium-browser>
<dsa 2015 3377 mysql-5.5>
<dsa 2015 3378 gdk-pixbuf>
<dsa 2015 3379 miniupnpc>
<dsa 2015 3380 php5>
<dsa 2015 3381 openjdk-7>
<dsa 2015 3382 phpmyadmin>
<dsa 2015 3384 virtualbox>
<dsa 2015 3385 mariadb-10.0>
<dsa 2015 3386 unzip>
<dsa 2015 3387 openafs>
<dsa 2015 3388 ntp>
<dsa 2015 3390 xen>
<dsa 2015 3391 php-horde>
<dsa 2015 3392 freeimage>
<dsa 2015 3393 iceweasel>
<dsa 2015 3394 libreoffice>
<dsa 2015 3395 krb5>
<dsa 2015 3397 wpa>
<dsa 2015 3398 strongswan>
<dsa 2015 3399 libpng>
<dsa 2015 3400 lxc>
<dsa 2015 3401 openjdk-7>
<dsa 2015 3402 symfony>
<dsa 2015 3403 libcommons-collections3-java>
<dsa 2015 3404 python-django>
<dsa 2015 3405 smokeping>
<dsa 2015 3406 nspr>
<dsa 2015 3407 dpkg>
<dsa 2015 3409 putty>
<dsa 2015 3411 cups-filters>
<dsa 2015 3412 redis>
<dsa 2015 3413 openssl>
<dsa 2015 3414 xen>
<dsa 2015 3415 chromium-browser>
<dsa 2015 3416 libphp-phpmailer>
<dsa 2015 3417 bouncycastle>
<dsa 2015 3418 chromium-browser>
<dsa 2015 3419 cups-filters>
<dsa 2015 3420 bind9>
<dsa 2015 3421 grub2>
<dsa 2015 3422 iceweasel>
<dsa 2015 3423 cacti>
<dsa 2015 3424 subversion>
<dsa 2015 3425 tryton-server>
<dsa 2015 3426 linux>
<dsa 2015 3427 blueman>
<dsa 2015 3428 tomcat8>
<dsa 2015 3429 foomatic-filters>
<dsa 2015 3430 libxml2>
<dsa 2016 3431 ganeti>
<dsa 2016 3433 ldb>
<dsa 2016 3433 samba>
<dsa 2016 3434 linux>
<dsa 2016 3435 git>
<dsa 2016 3438 xscreensaver>
<dsa 2016 3439 prosody>
<dsa 2016 3440 sudo>
<dsa 2016 3441 perl>
<dsa 2016 3442 isc-dhcp>
<dsa 2016 3443 libpng>
<dsa 2016 3444 wordpress>
<dsa 2016 3445 pygments>
<dsa 2016 3446 openssh>

</table>

<h2>Paquetes eliminados</h2>

<p>Se han eliminado los paquetes listados a continuación por circunstancias ajenas
a nosotros:</p>


<table border=0>
<tr><th>Paquete</th>               <th>Motivo</th></tr>

<correction core-network              "Problemas de seguridad">
<correction elasticsearch             "Ya no está soportado">
<correction googlecl                  "Roto por depender de API obsoletas">
<correction libnsbmp                  "Problemas de seguridad, sin desarrollo activo">
<correction libnsgif                  "Problemas de seguridad, sin desarrollo activo">
<correction vimperator                "Incompatible con versiones más recientes de iceweasel">
</table>

<h2>Instalador de Debian</h2>

Se ha actualizado el instalador para reincorporar el soporte de dispositivos QNAP TS-x09 y
para incluir las correcciones incorporadas por esta nueva versión en la distribución «estable».

<h2>URL</h2>

<p>Las listas completas de paquetes que han cambiado en esta
versión:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>La distribución «estable» actual:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>Actualizaciones propuestas a la distribución «estable»:</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>Información sobre la distribución «estable» (notas de publicación, erratas, etc.):</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>Información y anuncios de seguridad:</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>


<h2>Acerca de Debian</h2>

<p>El proyecto Debian es una asociación de desarrolladores de software libre que
aportan de forma voluntaria su tiempo y esfuerzo para producir el sistema operativo
Debian, un sistema operativo completamente libre.</p>


<h2>Información de contacto</h2>

<p>Para más información, visite las páginas web de Debian en
<a href="$(HOME)/">https://www.debian.org/</a>, envíe un correo electrónico a
&lt;press@debian.org&gt; o contacte con el equipo responsable de la publicación en
&lt;debian-release@lists.debian.org&gt;.</p>


